# -*- coding: utf-8 -*-
"""
Created on Tue Sep 18 18:29:46 2018

@author: Ruben.Mueller
"""

import json

### THIS DICT PROVIDES A RELATION BETWEEN WINDOWS USER AND POSTGRES USER + PASSWORT
### Beziehung zwischen Windows-Anmeldungsname und Postgresnutzer + Passwort
### USERMAP = {
###            'NUTERNAME': [POSTRESNUTZER, PASSWORT],
###            'NUTERNAME': [POSTRESNUTZER, PASSWORT],
###           }

USERFILE = 'C:\\Users\\xxx\\.qgis2\\python\\plugins\\LHW_Tools\\users.json'


USERMAP = {'default': ['whhviewer', 
                       'xxx'],
           'user1': ['postgres', 
                             'xxx'],
           'user2': ['whhadmin', 
                                    'xxx'],
           'Bearbeiterin 1': ['whh_user', 'xxx'],
           'Bearbeiterin 2': ['whh_user', 'xxx']}

with open(USERFILE, 'w') as fb:
    json.dump(USERMAP, fb)