# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Dialog for the water balance.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de

    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""
import processing
import os
from qgis.core import NULL
from qgis.PyQt.QtWidgets import QDialog
from qgis.PyQt.QtGui import QFont
from qgis.core import Qgis
from qgis.core import QgsProject
from qgis.PyQt.QtCore import QSettings
from qgis.core import QgsFeatureRequest
# from qgis.core import QgsDistanceArea
# from qgis.PyQt.QtCore import QVariant
# from qgis.core import QgsVectorLayer
# from qgis.core import QgsFeature
# from qgis.core import QgsField

from .tools.iterate_well2 import get_recharge_from_element
from .setup import setup__ as SETUP
from .pyqtDialogs.dialog_RO_balance import Ui_Dialog_RO_balance
from .ro_selectExtraction import Start_Dialog_RO_selectExtraction

def fill(entryName, unit=None, result=None):
    '''format an ouput line

    Parameters
    ----------
    entryName : str
        the name of the water balance component
    unit : str, optional
        the unit of the water balance component
    result : str or float, optional
        the quantity for the water balance component'''

    if entryName is None:
        entryName = ""

    if unit is None:
        entry =  u'{}'.format(entryName).ljust(37,' ')
    else:
        entry = u'{}'.format(entryName).ljust(30,' ') \
                + u'{}'.format(unit).rjust(7,' ')

    if not result is None:
        if isinstance(result, float):
            entry += u'{:6.3f}'.format(result).rjust(10,' ')
        else:
            entry += u'{}'.format(result).rjust(10,' ')
    return entry + '\n'


def checkv(v):
    return True if isinstance(v, (float, int)) else False


class Start_Dialog_RO_balance(QDialog, Ui_Dialog_RO_balance):
    '''class for the  dialog'''
    #signalAcc = pyqtSignal(list)

    def __init__(self, parent=None, userDict=None):
        QDialog.__init__(self, parent.mainWindow())
        self.setupUi(self)
        self.iface = parent
        self.userDict = userDict
        self.pushButton.clicked.connect(self.account)
        self.pushButton_2.clicked.connect(self.reset)
        self.textEdit.clear()
        self.textEdit.setCurrentFont(QFont("Courier New"))
        self.textEdit.setFontPointSize(8)

        self.selextr = Start_Dialog_RO_selectExtraction(parent, self.userDict)


    def closePlugin(self):
        self.close()


    def closeEvent(self, event):
       event.accept()


    def reset(self):
        '''clear the selection (otherwise we have will problems
        when doing the selection multiple times)'''

        self.userDict['uIC'][0][-2] = [True for x in self.userDict['uIC'][0][-2]]
        self.userDict['uIC'][0][-1] = [False for x in self.userDict['uIC'][0][-1]]
        message = u'Auswahl zurückgesetzt. Bilanzierung mit neuer Auswahl starten.'
        self.iface.messageBar().pushMessage(message,
            level=Qgis.Warning, duration=7)
        return


    def account(self):
        '''calculate the water balance for the selection show the results'''
        self.userDict['account_text'] = ''
        self.textEdit.clear()
        try:
            float(self.userDict['BIL_JAHR_TM3'])
        except:
            message = 'Achtung Entnahmerate nicht definiert!'
            self.iface.messageBar().pushMessage(message,
                level=Qgis.Warning, duration=7)
            return

        self.userDict['TM3_JAHR total'] = 0
        self.userDict['results_GWN'].update({'RG1': 0, 'RG2': 0, 'RD': 0, 'RH': 0})
        self.userDict['GWR total'] = 0

        numItems = self.comboBox.count()
        ### only if catchments are given
        if numItems > 0:
            layerNameWell = self.comboBox.currentText()
            layerNameWHH = self.comboBox_2.currentText()
            print(layerNameWell, layerNameWHH)
            extrTotal = 0

            try:
                catch_layer = QgsProject.instance().mapLayersByName(layerNameWell)[0]
            except:
                fmtstr = u"Ungültiger Layer gesetzt."
                self.iface.messageBar().pushMessage(fmtstr,
                    level=Qgis.Warning, duration=7)
                return


            num_feats = len([x for x in catch_layer.getFeatures()])
            extractions = []
            stats = []
            results = []
            area_ges = 0.00001

            for i, feat in enumerate(catch_layer.getFeatures()):
                print(i, feat.geometry().area())

                if not self.userDict is None \
                or self.userDict['balanceCatchment'] == '(nicht gesetzt)':
                    extrTotal, tmp = self.calculateTotalExtraction(catch_layer, feat)
                    extractions.append(extrTotal)
                    stats.append(tmp)

                    self.userDict['uIC'].append(tmp)
                    self.userDict['TM3_JAHR total'] += extrTotal

                    ### dialog for selection of other users intersecting the catchment

                    ### do accounting
                    restmp = self.calculateBalance(layerNameWell,
                        feat, layerNameWHH,
                        self.userDict['gwRechargeComponents'])
                    
                    area_ges += restmp['Area']
                    results.append(restmp)

                    ### generate text
                    self.selextr.popTable(i)
                    self.selextr.exec_()

                    if num_feats > 1:
                        header = '\n-----------\n|Fläche {} |\n-----------\n'.format(i)
                    else:
                        header = ''
            
            for i, feat in enumerate(catch_layer.getFeatures()):
                    restmp = results[i]
                    print("------", restmp['failed_intersect'])
                    self.userDict['failed_intersect'] = restmp['failed_intersect']
                    text = header + self.generateText(restmp, i, area_ges, num_feats) + '\n'
                    for comp in ('RG1', 'RG2', 'RD', 'RH'):
                        self.userDict['results_GWN'][comp] += restmp[comp]
                    self.userDict['GWR total'] += restmp['GWR']
                    
                    self.userDict['account_text'] += text + '\n\n'
                    self.userDict['results_GWN']['Area'] = area_ges
            self.textEdit.setPlainText(self.userDict['account_text'])



    def calculateBalance(self, catch_layer, feat, layerNameWHH, check_wert):
        '''loop over all selected features to compute
         area weighted water balance components

         Parameters
         ----------
         catch_layer : layer
             layer of the well catchment
         feat : feature
             the feature of a multi-polygon, part of the catch_layer
         layerNameWHH : str
             the water balance layer name
         check_wert : list of bool
             the activated compartiments

         Returns
         -------
         dict with the computed results and total area.
         the keys are results and area'''

        if layerNameWHH == 'Aktiver Layer':
            efl_layer = self.iface.activeLayer()
        else:
            try:
                efl_layer = QgsProject.instance().mapLayersByName(layerNameWHH)[0]#SETUP.EFLLAYER
            except:
                fmtstr = u"Ungültiger Layer gesetzt."
                self.iface.messageBar().pushMessage(fmtstr,
                                                    level=Qgis.Warning, duration=7)
                return

        if isinstance(catch_layer, str):
            catch_layer = QgsProject.instance().mapLayersByName(catch_layer)[0]

        QSettings()
        
        results = get_recharge_from_element(feat.geometry(), 
             check_wert, efl_layer=efl_layer, 
             create_layer=SETUP.INTERSECT_FAIL_LAYER)
        return results



    def calculateTotalExtraction(self, catch_layer, feat):
        '''look if there are other extractions in the vicinity
        (intersecting with the catchment) and
        add them for a total extraction rate.

        Parameters
        ----------
        layerName : str
            name of the catchment layer

        Returns
        -------
        total_extr : float
            the total extraction rate'''

        name = []
        adr_vorname = []
        adr_name = []
        rate = []
        art = []
        zweck = []
        nachtrag = []
        regnr1 = []

        QSettings()

        total_extr = 0

        def san(item):
            tmp = '-' if item == NULL else item  ### QPyNullVariant is gone
            return tmp

        print('starting selction of wells in catchment')
        for layername in ('wnv_land', 'wnv_lhw'):
            lname = SETUP.DB_CONF[layername]['table']
            efl_layer = QgsProject.instance().mapLayersByName(lname)[0]
            #print('selection for ', layername)
            id_extr = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]["BIL_JAHR_TM3"])
            id_name = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]['applicationNumber'])
            if layername == 'wnv_land':
                id_adr_vorname = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]['userVorName'])
                id_art = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]['nutzart'])
                id_zweck = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]['nutzzweck'])
            id_adr_name = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]['userName'])

            id_nachtrag = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]['nachtrag'])
            id_reg_nr2 = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]['regnr1'])
            #print(feat.geometry())
            #cands = efl_layer.getFeatures(QgsFeatureRequest().setFilterRect(feat.geometry().boundingBox()))

            clipx = processing.run("native:clip", { 'INPUT': efl_layer,
                'OUTPUT' : 'memory:',
                'OVERLAY' : catch_layer})['OUTPUT']
            #print(clipx.getFeatures())
            try:
                for i, area_feature in enumerate(clipx.getFeatures()):
                    efl_Geometrie = area_feature.geometry()
                    efl_Attributte = area_feature.attributes()
                    #print(efl_Geometrie, efl_Attributte)
                    if efl_Geometrie.intersects(feat.geometry()):
                        print('intersect', i, efl_Attributte[id_extr], efl_Attributte[id_name])
                        if not efl_Attributte[id_extr] == NULL:  ### QPyNullVariant is gone
                            total_extr += efl_Attributte[id_extr]
                            name.append(san(efl_Attributte[id_name]))

                            if layername == 'wnv_land':
                                tmp = efl_Attributte[id_adr_vorname] if id_adr_vorname > -1 else '-'
                            else:
                                tmp = '-'
                            adr_vorname.append(san(tmp))

                            tmp = efl_Attributte[id_adr_name] if id_adr_name > -1 else '-'
                            adr_name.append(san(tmp))

                            tmp = efl_Attributte[id_extr] if id_extr > -1 else '-'
                            rate.append(san(tmp))

                            if layername == 'wnv_land':
                                tmp = efl_Attributte[id_art] if id_art > -1 else '-'
                            else:
                                tmp = '-'
                            art.append(san(tmp))

                            if layername == 'wnv_land':
                                tmp = efl_Attributte[id_zweck] if id_zweck > -1 else '-'
                            else:
                                tmp = '-'
                            zweck.append(san(tmp))

                            tmp = efl_Attributte[id_nachtrag] if id_nachtrag > -1 else '-'
                            nachtrag.append(san(tmp))

                            tmp = efl_Attributte[id_reg_nr2] if id_reg_nr2 > -1 else '-'
                            regnr1.append(san(tmp))
                            #print('-->', i, area_feature)
                            #print('-->', i, '---', total_extr, name, adr_name, adr_vorname, rate, zweck, art, nachtrag, regnr1)
                    else:
                        print('NULL@', i, area_feature)
            except TypeError:
                 fmtstr = u"""Bitte die SETUP-Datei auf korrekte Zuordnung in wnv_land und wnv_lhw prüfen.
                 [BIL_JAHR_TM3, applicationNumber, userVorName, userName, nutzart, nutzzweck, nachtrag, regnr1]"""
                 self.iface.messageBar().pushMessage(fmtstr, level=Qgis.Warning, duration=7)

        extraction = [True for i in name]
        selection = [False for i in name]
        #print('total_extr', total_extr)
        return total_extr, [name, adr_name, adr_vorname, rate, zweck, art, nachtrag, regnr1, selection, extraction]


    def generateText(self, results, num, area_ges, num_feats):
        '''format an string for the textEdit

        Parameters
        ----------
        results : dict
            the water balance from calculateBalance
        extrTotal : float
            the total extraction rate from calculateTotalExtraction

        Returns
        -------
        the formated results string : str'''
        ### legal stuff
        #results = results_list[area_num]

        string = fill('Nutzer', result=self.userDict['userName'])
        string += fill('Antragsnummer',
                       result=self.userDict['applicationNumber'])
        ### area and Q
        string += fill(u'Bezugsgebietsfläche', unit=u'     [km²]',
                       result=results['Area'] / 1000000)
        string += fill(u'QL',  unit=u' [l/s/km²]',
                       result=self.userDict['QL'])
        string += '-'*57 + '\n'

        if area_ges == 0:
            area_ges = 0.0001
        if results['Area'] == 0:
            results['Area'] = 0.0001

        if num_feats > 1:
            tmp = 'Entnahme (Flächenanteil)'
        else:
            tmp = 'Entnahme lokal'
        try:
            string += fill(tmp, unit=u'[Tm³/Jahr]',
                result=float(self.userDict['BIL_JAHR_TM3']) * float(results['Area']) / float(area_ges))
        except Exception as e:
            fmtstr = u"""Seltener Fehler. Bitte diesen Text an das BAH schicken:
                Error 01: {}, {}, {} ::: {}, {}, {} -- {}""".format(self.userDict['BIL_JAHR_TM3'],
                results['Area'], area_ges, type(self.userDict['BIL_JAHR_TM3']),
                type(results['Area']), type(area_ges), e)
            self.iface.messageBar().pushMessage(fmtstr, level=Qgis.Warning, duration=60)
            return


        ### Q from intersecting well catchments
        for i, use in enumerate(self.userDict['uIC'][num][-2]):
            if use and not self.userDict['uIC'][num][0][i].encode(encoding=SETUP.ENCODING)  \
            == self.userDict['applicationNumber'].encode(encoding=SETUP.ENCODING):
                if self.userDict['uIC'][num][-1][i]:
                    res = self.userDict['uIC'][num][3][i] * -1
                else:
                    res = self.userDict['uIC'][num][3][i]
                string += fill(self.userDict['uIC'][num][0][i], unit=u'[Tm³/Jahr]',
                               result=res)

        ### additional balance
        for i, namex in enumerate(self.userDict['names_addBalance']):
            if len(namex) > 0:
                string += fill(namex,  unit=u'[Tm³/Jahr]',
                       result=self.userDict['list_addBalance'][i])

        #print(type(self.userDict['JAHR_TM3']), type(self.userDict['total_addBalance']))
        ### the sum of all
        sumx = float(self.userDict['BIL_JAHR_TM3']) * results['Area'] / area_ges \
               + float(self.userDict['total_addBalance']) \
               + self.userDict['TM3_JAHR uIC']

        string += '-'*57 + '\n'
        string += fill('Summe Entnahmen',  unit=u'[Tm³/Jahr]',
                       result=sumx) + '\n'

        if results['checks'][0]:
            string += fill('RG2', unit=u'[Tm³/Jahr]',
                           result=results['RG2']).strip() \
                           + ' '*2 + '= ' \
                           + u'{:3.1f}'.format(results['RG2']/results['Area']*1000000) \
                           + u' mm/a\n'
        if results['checks'][1]:
            string += fill('RG1', unit=u'[Tm³/Jahr]',
                           result=results['RG1']).strip() \
                           + ' '*2 + '= ' \
                           + u'{:3.1f}'.format(results['RG1']/results['Area']*1000000) \
                           + u' mm/a\n'
        if results['checks'][2]:
            string += fill('RD', unit=u'[Tm³/Jahr]',
                                   result=results['RD']).strip() \
                           + ' '*2 + '= ' \
                           + u'{:3.1f}'.format(results['RD']/results['Area']*1000000) \
                           + u' mm/a\n'
        if results['checks'][3]:
            string += fill('RH', unit=u'[Tm³/Jahr]',
                                   result=results['RH']).strip() \
                           + ' '*2 + '= ' \
                           + u'{:3.1f}'.format(results['RH']/results['Area']*1000000) \
                           + u' mm/a\n'

        string += '-'*57 + '\n'
        string += fill('Summe ist GWN',  unit=u'[Tm³/Jahr]',
                       result=results['GWR'])

        string += '='*57 + '\n'*2

        balance = results['GWR'] - sumx

        if abs(balance) <= SETUP.EPSBALANCE:
            tempa = u'Entnahme und GWN sind jeweils ' \
                       u'{:6.2f}'.format(self.userDict['BIL_JAHR_TM3'] * results['Area'] / area_ges) + ' [Tm³/Jahr]. Bilanz ist ausgeglichen.'
            string += u'Entnahme und GWN sind jeweils ' \
                       + u'{:6.2f}'.format(self.userDict['BIL_JAHR_TM3'] * results['Area'] / area_ges) + ' [Tm³/Jahr]. Bilanz ist ausgeglichen.'
        else:
            temp = u' HÖHER ' if balance > SETUP.EPSBALANCE else u' NIEDRIGER '
            tempa = u'GWN ist ' + u'{:6.2f}'.format(abs(balance)) + u' [Tm³/Jahr]' + temp + 'als Entnahme(n)'
            string += u'GWN ist ' + u'{:6.2f}'.format(abs(balance)) + u' [Tm³/Jahr]' + temp + 'als Entnahme(n)'
        self.userDict['bilance_text'] = tempa
            
        if self.userDict['failed_intersect']:
           string += "\n\n\n-------------------------\n" 
           string += "IDs von Flächen mit\nnicht verschneidbarer Geometrie" 
           string += "\n-------------------------\n" 
           string += "\n".join([str(x) for x in self.userDict['failed_intersect']])
           string += "\n-------------------------\n" 
              

        ### total area, if more then one sub area
        if num_feats > 1:
            pass
        return string

