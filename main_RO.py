# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Dialog to select how to start the dialog for user management and reasoned statement.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

import os
import sys
import datetime

from qgis.PyQt.QtWidgets import QDialog
from qgis.core import QgsProject
from qgis.core import QgsFeatureRequest
from qgis.PyQt.QtWidgets import QFileDialog
from qgis.PyQt.QtWidgets import QApplication

from qgis.core import Qgis

from .tools import qgis_toolbox
from .setup import setup__ as SETUP
from .tools import database_toolbox as db_toolbox
from .tools import create_userdict as create_ud
from .ad_delete import Start_admin_delete

from .pyqtDialogs.dialog_RO_main import Ui_Dialog_RO_main
from .ro_userManagement import Start_Dialog_RO_userManagement
from .ro_chooseUser import Start_Dialog_chooseUser

DEBUG = False #True # 
###############
# default dict for all plugin data --> the "user dictionary"
###############
### TODO: clear entries after debug



class Start_Dialog_RO_main(QDialog, Ui_Dialog_RO_main):
    '''class for the main dialog of the reasoned opinion builder'''

    #signalExtrRead = pyqtSignal()


    def __init__(self, parent=None):
        super().__init__(parent.mainWindow())
        self.setupUi(self)
        self.iface = parent
        self.checkUser()
        self.pushButton_01.clicked.connect(self.newUser)
        self.pushButton_02.clicked.connect(self.permanentUser)
        self.pushButton_03.clicked.connect(self.temporaryUser)
        self.pushButton_ad_delete.clicked.connect(self.delete_show)
        self.nv = None
        self.db_rw = None


    def closePlugin(self):
        self.close()
        
        
    def delete_show(self):
        self.setDB()
        self.admin_delete = Start_admin_delete(self.iface, self.db_rw)
        self.admin_delete.show()


    def permanentUser(self):
        '''select an user from the WELL layer,
        load it's data and open the dialog of the reasoned opinion'''
        # select from layer
        self.setDB()
        qgis_toolbox.load_layer(SETUP.TGLAYER, set_active=False)
        layerT = QgsProject.instance().mapLayersByName(SETUP.TGLAYER)[0]
        layerW = self.iface.activeLayer()
        if not layerW:
            message = u"Bitte Nutzer von gültigem Layer selektieren."
            self.iface.messageBar().pushMessage(message,
                level=Qgis.Info, duration=7)
            return
        selectedFeatures = layerW.selectedFeatures()
        errorFlag = False

        ### ensure selection is done
        if not (layerW.name() == SETUP.DB_CONF['wnv_lhw']['table'] \
                or layerW.name() == SETUP.DB_CONF['wnv_land']['table']):
            message = u"Bitte Nutzer von gültigem Layer selektieren."
            errorFlag = True
        elif len(selectedFeatures) == 0:
            message = "0 Nutzer selektiert. Bitte eine Selektion vornehmen."
            errorFlag = True
        elif len(selectedFeatures) > 1:
            chsr = Start_Dialog_chooseUser(self.iface)
            chsr.exec_()
            selectedFeatures = layerW.selectedFeatures()

        ### print warning if necessary (selection available?)
        if errorFlag:
            self.iface.messageBar().pushMessage(message,
                level=Qgis.Info, duration=7)
            return
        else:
            ### read user specifics from well layer
            userDict = qgis_toolbox.read_selectedUser(layerW, 
                selectedFeatures, create_ud.createUserContainer(DEBUG, SETUP))
            userDict['fromLayer'] = layerW.name()
            
            ### in case that new coordinates get assigned, 
            # keep initial coordinates for comparison (write to DB)
            userDict['coordinateInitX'] = userDict['coordinateX']
            userDict['coordinateInitY'] = userDict['coordinateY']
            
            #### set currentStatus to internal coding accordingly
            if userDict['currentStatusT'] == "in Bearbeitung":
                userDict['currentStatus'] = 0
            elif userDict['currentStatusT'] == "abgelehnt":
                userDict['currentStatus'] = 1
            else:
                userDict['currentStatus'] = 2

            if layerW.name() == SETUP.DB_CONF['wnv_lhw']['table']:           
                try:
                    ### id of user in wnv_lhw
                    idx_id = layerW.fields().lookupField(SETUP.DB_CONF['wnv_lhw']['id'])
                    if isinstance(selectedFeatures, list):
                        selectedFeatures = selectedFeatures[0]
                    curr_id = selectedFeatures.attributes()[idx_id]
                    
                    ### request feature with id in ezg
                    layer_ezg = QgsProject.instance().mapLayersByName(SETUP.DB_CONF['ezg']['table'])[0]
                    req = '"{}" = {}'.format(SETUP.DB_CONF['ezg']['id'], curr_id)
                    request = QgsFeatureRequest().setFilterExpression(req)
                    feat_ezg_l = layer_ezg.getFeatures(request)
                    
                    feat_ezg = list(feat_ezg_l)[0]

                    for key in ('DAY_M3', 'days'):
                        idx_d = layer_ezg.fields().lookupField(SETUP.DB_CONF['ezg'][key])
                        userDict[key] = feat_ezg.attributes()[idx_d]

                except Exception:
                    message = "Auswahl ohne Auswertung des EZG-Layers. Ist dieser geladen?"
                    self.iface.messageBar().pushMessage(message,
                        level=Qgis.Info, duration=7)
            
            #print(userDict['BIL_JAHR_TM3'])
            userDict = self.getNewUserQL(userDict,
                layerT, selectedFeatures)
            #print('perm user dict  {}'.format(userDict))
            self.nv = Start_Dialog_RO_userManagement(self.iface,
                 userDict, userDict, 1, self.db_rw)
            self.nv.show()


    def temporaryUser(self):
        '''select an working process snatshot for an unfinished reasoned opinion
        load it's data and open the dialog of the reasoned opinion'''
        # select folder
        self.setDB()
        fnameC = QFileDialog.getExistingDirectory(self,
            'Temporaeren Nutzer laden', SETUP.TEMPFOLDER)
        if len(fnameC) == 0:
            return
        userDict = qgis_toolbox.load_obj_compressed(os.path.join(fnameC,
                                                                 'project'))
        userDict['tempPath'] = fnameC
        #print('loaded  {}'.format(userDict))
        self.nv = Start_Dialog_RO_userManagement(self.iface,
            userDict, userDict, 2, self.db_rw)
        try:
            qgis_toolbox.load_layer('BrunnenEGZ', 
                set_active=False,  folderx=fnameC, 
                altName=userDict['wellCatchment'])
        except:
            message = 'Konnte Brunneneinzugsgebiet nicht laden.'
            self.iface.messageBar().pushMessage(message,
                level=Qgis.Info, duration=3)
        try:
            qgis_toolbox.load_layer('Bilanzgebiet', 
                set_active=False, folderx=fnameC, 
                altName=userDict['balanceCatchment'])
        except:
            message = 'Konnte Bilanzierungsgebiet nicht laden.'
            self.iface.messageBar().pushMessage(message,
                level=Qgis.Info, duration=3)
            
        self.nv.show()


    def newUser(self):
        '''create an new user'''
        self.setDB()
        self.nv = Start_Dialog_RO_userManagement(self.iface,
            create_ud.createUserContainer(DEBUG, SETUP), create_ud.createUserContainer(DEBUG, SETUP),
            0, self.db_rw)
        self.nv.show()


    def getNewUserQL(self, userDict, tgLayer, selectedFeatures):
        '''read the QL value for an user from the WELL layer

        Parameters
        ----------
        userDict : dict
            the user dictionary
        tgLayer : layer
            sub catchment layer
        selectedFeatures : selectedFeatures object
            the selected well

        Returns
        -------
        updated user dictionary : dict'''
        request = QgsFeatureRequest().setFilterExpression(u'"TGID" = ' + str(userDict['TG']))
        itL = tgLayer.getFeatures(request)
        idx_ql = tgLayer.fields().lookupField("QL_SPENDE")
        idx_qleff = tgLayer.fields().lookupField("QL_EFF_SPE")
        for it in itL:
            userDict['QL'] = it.attributes()[idx_ql]
            userDict['QL_eff'] = it.attributes()[idx_qleff]
            break
        return userDict


    def setDB(self):
        self.db_rw = db_toolbox.userDB('rw')
        self.db_rw.table_to_layer(SETUP.DB_CONF['ist_mengen']['table'],
            SETUP.DB_CONF['ist_mengen']['table'])
        self.db_rw.table_to_layer(SETUP.DB_CONF['ezg']['table'],
            SETUP.DB_CONF['ezg']['table'])
        self.db_rw.table_to_layer(SETUP.DB_CONF['wnv_land']['table'],
            SETUP.DB_CONF['wnv_land']['table'])
        self.db_rw.table_to_layer(SETUP.DB_CONF['wnv_lhw']['table'],
            SETUP.DB_CONF['wnv_lhw']['table'])


    def checkUser(self):
        enable = False
        try:
            winUser = os.getenv('username')
            if winUser in ("schneppmueller", "ruben.mueller"):
                enable = True
        except Exception as e:
            print("AD delete: " + e)
        finally:
            self.pushButton_ad_delete.setEnabled(enable)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = Start_Dialog_RO_main(None)
    w.show()
    sys.exit(app.exec_())
