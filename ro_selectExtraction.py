# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Dialog to define the end points of a well catchment.
    An Endpoint can be defined either by the active point layer (with a single
    endpoint with a name from SETUP.ENDPOINT), or by giving the X and Y coorinates.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

from qgis.core import Qgis
from qgis.core import NULL
from qgis.PyQt import QtCore
from qgis.PyQt.QtWidgets import QDialog
from qgis.PyQt.QtWidgets import QTableWidgetItem
from qgis.PyQt.QtGui import QStandardItemModel
from .pyqtDialogs.dialog_RO_selectExtraction import Ui_Dialog_RO_selectExtraction
from .setup import setup__ as SETUP


NUMENTRY = 10


def check_encoding2(part):
    part = part.replace('\\xf6', '&ouml;')#[2:-1]
    part = part.replace('\\xe4', '&auml')#[2:-1]
    part = part.replace('\\xfc', '&uuml')#[2:-1]
    part = part.replace('\\xdf', '&szlig')#[2:-1]
    return part

class Start_Dialog_RO_selectExtraction(QDialog, Ui_Dialog_RO_selectExtraction):
    '''class for the dialog to define the end points of a well catchment'''


    def __init__(self, parent=None, dictP=None):
        QDialog.__init__(self, parent.mainWindow())
        self.setupUi(self)
        self.iface = parent
        self.dictP = dictP
        self.num = 0
        self.pushButton.clicked.connect(self.startSelection)
        
        self._headerData = (QtCore.QVariant("Aktenzeichen"), 
                            QtCore.QVariant(u"Rate [Tm³/Jahr]"), 
                            QtCore.QVariant("Zweck"),
                            QtCore.QVariant("Art"),
                            QtCore.QVariant("Nachtrag"),
                            QtCore.QVariant("Reg.Nr. 1"),
                            QtCore.QVariant(u"Übernahme"),
                            QtCore.QVariant("Einleitung")
                           )

    def clearTable(self):
        '''clear the table for reuse'''
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(NUMENTRY)


    def popTable(self, num):
        '''populate the table
        
        Parameters
        ----------
        num : int
            current subarea'''
        ### set the table
        
        nb_row = len(self.dictP['uIC'][num][0])
        qTable = self.tableWidget
        qTable.setRowCount(nb_row)
        qTable.setColumnCount(NUMENTRY)
        chkBoxItem = QTableWidgetItem()
        qTable.setHorizontalHeaderLabels(['Aktenzeichen', 'Name', 'Vorname',
            'u"Rate [Tm³/Jahr]"', 'Zweck', 'Art',
            'Nachtrag', 'Reg.Nr. 1', u'Übernahme', 'Einleitung'])
        
        for row in range(nb_row):
            
            if not self.dictP['uIC'][num][0][row].encode(encoding=SETUP.ENCODING)  \
            == self.dictP['applicationNumber'].encode(encoding=SETUP.ENCODING):
            
                for col in range(8):
                    item = QTableWidgetItem(QTableWidgetItem(self.san_t(col, row, num)))
                    qTable.setItem(row, col, item)
                for col in (8, 9):
                    chkBoxItem = QTableWidgetItem()
                    chkBoxItem.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
                    chkBoxItem.setCheckState(QtCore.Qt.Checked) if col == 8 else chkBoxItem.setCheckState(QtCore.Qt.Unchecked)
                    qTable.setItem(row, col, chkBoxItem)
        
    def san_t(self, n, i, num):
        '''correct encoding for each entry. Nasty hack...'''
        tmp = '?'
        print(self.dictP['uIC'][num][n][i] , n, i, num)
        if self.dictP['uIC'][num][n][i] == NULL:
            return '?'
        else:
            try:
                tmp = check_encoding2(str(self.dictP['uIC'][num][n][i]))
            except:
                tmp = str(self.dictP['uIC'][num][n][i])

        return tmp


    def startSelection(self):
        '''read table and include entry in balance or not'''
        addExtr = 0
        for i, name in enumerate(self.dictP['uIC'][self.num][0]):
            if not self.dictP['uIC'][self.num][0][i].encode(encoding=SETUP.ENCODING)  \
            == self.dictP['applicationNumber'].encode(encoding=SETUP.ENCODING):
                ### read
                rate = float(self.dictP['uIC'][self.num][3][i])
                use = True if self.tableWidget.item(i, NUMENTRY-2).checkState() == QtCore.Qt.Checked else False
                plumi = True if self.tableWidget.item(i, NUMENTRY-1).checkState() == QtCore.Qt.Checked else False

                ### save to userDict
                self.dictP['uIC'][self.num][-2][i] = use
                self.dictP['uIC'][self.num][-1][i] = plumi
                
                if use:
                    add = rate * -1 if plumi else rate
                    addExtr += add

        self.dictP['TM3_JAHR uIC'] = addExtr
        self.close()

    def closeEvent(self, event):
       event.accept()
