# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_AE_startAE.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog_AE_startAE(object):
    def setupUi(self, Dialog_AE_startAE):
        Dialog_AE_startAE.setObjectName("Dialog_AE_startAE")
        Dialog_AE_startAE.resize(263, 253)
        self.verticalLayoutWidget = QtWidgets.QWidget(Dialog_AE_startAE)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 241, 231))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(9)
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.line = QtWidgets.QFrame(self.verticalLayoutWidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName("formLayout")
        self.label_2 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.dateEditStart = QtWidgets.QDateEdit(self.verticalLayoutWidget)
        self.dateEditStart.setMinimumSize(QtCore.QSize(0, 24))
        self.dateEditStart.setObjectName("dateEditStart")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.dateEditStart)
        self.label_3 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.dateEditEnd = QtWidgets.QDateEdit(self.verticalLayoutWidget)
        self.dateEditEnd.setMinimumSize(QtCore.QSize(0, 24))
        self.dateEditEnd.setObjectName("dateEditEnd")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.dateEditEnd)
        self.verticalLayout.addLayout(self.formLayout)
        self.line_2 = QtWidgets.QFrame(self.verticalLayoutWidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout.addWidget(self.line_2)
        self.label_4 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_4.setObjectName("label_4")
        self.verticalLayout.addWidget(self.label_4)
        self.label_5 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_5.setObjectName("label_5")
        self.verticalLayout.addWidget(self.label_5)
        self.lineEditVariant = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.lineEditVariant.setMinimumSize(QtCore.QSize(0, 24))
        self.lineEditVariant.setObjectName("lineEditVariant")
        self.verticalLayout.addWidget(self.lineEditVariant)
        self.line_3 = QtWidgets.QFrame(self.verticalLayoutWidget)
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.verticalLayout.addWidget(self.line_3)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.pushButtonStart = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButtonStart.setMinimumSize(QtCore.QSize(128, 24))
        self.pushButtonStart.setObjectName("pushButtonStart")
        self.horizontalLayout_2.addWidget(self.pushButtonStart)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(Dialog_AE_startAE)
        QtCore.QMetaObject.connectSlotsByName(Dialog_AE_startAE)

    def retranslateUi(self, Dialog_AE_startAE):
        _translate = QtCore.QCoreApplication.translate
        Dialog_AE_startAE.setWindowTitle(_translate("Dialog_AE_startAE", "Dialog"))
        self.label.setText(_translate("Dialog_AE_startAE", "ArcEGMO-Rechnung"))
        self.label_2.setText(_translate("Dialog_AE_startAE", "Berechnungsbeginn"))
        self.label_3.setText(_translate("Dialog_AE_startAE", "Berechnungsende  "))
        self.label_4.setText(_translate("Dialog_AE_startAE", "Name der Berechnungsvariante:"))
        self.label_5.setText(_translate("Dialog_AE_startAE", "(Ganzes Wort ohne Umlaute und Leerzeichen)"))
        self.pushButtonStart.setText(_translate("Dialog_AE_startAE", "Rechnung starten"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog_AE_startAE = QtWidgets.QDialog()
    ui = Ui_Dialog_AE_startAE()
    ui.setupUi(Dialog_AE_startAE)
    Dialog_AE_startAE.show()
    sys.exit(app.exec_())
