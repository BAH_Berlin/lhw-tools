# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_AE_gwRelocation.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog_AE_gwRelocation(object):
    def setupUi(self, Dialog_AE_gwRelocation):
        Dialog_AE_gwRelocation.setObjectName("Dialog_AE_gwRelocation")
        Dialog_AE_gwRelocation.resize(382, 241)
        self.gridLayout = QtWidgets.QGridLayout(Dialog_AE_gwRelocation)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.Bezeichnung_txt_2 = QtWidgets.QLabel(Dialog_AE_gwRelocation)
        font = QtGui.QFont()
        font.setPointSize(9)
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.Bezeichnung_txt_2.setFont(font)
        self.Bezeichnung_txt_2.setObjectName("Bezeichnung_txt_2")
        self.verticalLayout.addWidget(self.Bezeichnung_txt_2)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(Dialog_AE_gwRelocation)
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.pushButton = QtWidgets.QPushButton(Dialog_AE_gwRelocation)
        self.pushButton.setMinimumSize(QtCore.QSize(128, 0))
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_2.addWidget(self.pushButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.line = QtWidgets.QFrame(Dialog_AE_gwRelocation)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_2 = QtWidgets.QLabel(Dialog_AE_gwRelocation)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.pushButton_2 = QtWidgets.QPushButton(Dialog_AE_gwRelocation)
        self.pushButton_2.setMinimumSize(QtCore.QSize(128, 0))
        self.pushButton_2.setObjectName("pushButton_2")
        self.horizontalLayout.addWidget(self.pushButton_2)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem2)
        self.pushButton_3 = QtWidgets.QPushButton(Dialog_AE_gwRelocation)
        self.pushButton_3.setMinimumSize(QtCore.QSize(128, 0))
        self.pushButton_3.setObjectName("pushButton_3")
        self.horizontalLayout_4.addWidget(self.pushButton_3)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.line_2 = QtWidgets.QFrame(Dialog_AE_gwRelocation)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout.addWidget(self.line_2)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem3)
        self.pushButton_4 = QtWidgets.QPushButton(Dialog_AE_gwRelocation)
        self.pushButton_4.setMinimumSize(QtCore.QSize(128, 0))
        self.pushButton_4.setObjectName("pushButton_4")
        self.horizontalLayout_5.addWidget(self.pushButton_4)
        self.verticalLayout.addLayout(self.horizontalLayout_5)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_AE_gwRelocation)
        QtCore.QMetaObject.connectSlotsByName(Dialog_AE_gwRelocation)

    def retranslateUi(self, Dialog_AE_gwRelocation):
        _translate = QtCore.QCoreApplication.translate
        Dialog_AE_gwRelocation.setWindowTitle(_translate("Dialog_AE_gwRelocation", "Dialog"))
        self.Bezeichnung_txt_2.setText(_translate("Dialog_AE_gwRelocation", "Grundwasserverlagerung"))
        self.label.setText(_translate("Dialog_AE_gwRelocation", "Bestehende GW-Verlagerungen darstellen"))
        self.pushButton.setText(_translate("Dialog_AE_gwRelocation", "Darstellen"))
        self.label_2.setText(_translate("Dialog_AE_gwRelocation", "Neue GW-Verlagerung hinzufügen"))
        self.pushButton_2.setText(_translate("Dialog_AE_gwRelocation", "Pfeilwerkzeug aktivieren"))
        self.pushButton_3.setText(_translate("Dialog_AE_gwRelocation", "Direkteingabefenster"))
        self.pushButton_4.setText(_translate("Dialog_AE_gwRelocation", "Anwenden"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog_AE_gwRelocation = QtWidgets.QDialog()
    ui = Ui_Dialog_AE_gwRelocation()
    ui.setupUi(Dialog_AE_gwRelocation)
    Dialog_AE_gwRelocation.show()
    sys.exit(app.exec_())
