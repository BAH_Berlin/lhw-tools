# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_RO_main.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog_RO_main(object):
    def setupUi(self, Dialog_RO_main):
        Dialog_RO_main.setObjectName("Dialog_RO_main")
        Dialog_RO_main.resize(385, 114)
        self.gridLayout_3 = QtWidgets.QGridLayout(Dialog_RO_main)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.line_2 = QtWidgets.QFrame(Dialog_RO_main)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.gridLayout_3.addWidget(self.line_2, 2, 0, 1, 1)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.pushButton_01 = QtWidgets.QPushButton(Dialog_RO_main)
        self.pushButton_01.setMinimumSize(QtCore.QSize(150, 24))
        self.pushButton_01.setObjectName("pushButton_01")
        self.gridLayout.addWidget(self.pushButton_01, 1, 0, 1, 1)
        self.pushButton_03 = QtWidgets.QPushButton(Dialog_RO_main)
        self.pushButton_03.setMinimumSize(QtCore.QSize(200, 24))
        self.pushButton_03.setObjectName("pushButton_03")
        self.gridLayout.addWidget(self.pushButton_03, 2, 2, 1, 1)
        self.pushButton_02 = QtWidgets.QPushButton(Dialog_RO_main)
        self.pushButton_02.setMinimumSize(QtCore.QSize(200, 24))
        self.pushButton_02.setObjectName("pushButton_02")
        self.gridLayout.addWidget(self.pushButton_02, 1, 2, 1, 1)
        self.line = QtWidgets.QFrame(Dialog_RO_main)
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 1, 1, 1, 1)
        self.line_3 = QtWidgets.QFrame(Dialog_RO_main)
        self.line_3.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.gridLayout.addWidget(self.line_3, 2, 1, 1, 1)
        self.gridLayout_3.addLayout(self.gridLayout, 1, 0, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.pushButton_ad_delete = QtWidgets.QPushButton(Dialog_RO_main)
        self.pushButton_ad_delete.setObjectName("pushButton_ad_delete")
        self.horizontalLayout_2.addWidget(self.pushButton_ad_delete)
        self.gridLayout_3.addLayout(self.horizontalLayout_2, 3, 0, 1, 1)

        self.retranslateUi(Dialog_RO_main)
        QtCore.QMetaObject.connectSlotsByName(Dialog_RO_main)

    def retranslateUi(self, Dialog_RO_main):
        _translate = QtCore.QCoreApplication.translate
        Dialog_RO_main.setWindowTitle(_translate("Dialog_RO_main", "Bearbeitung Stellungnahme"))
        self.pushButton_01.setText(_translate("Dialog_RO_main", "Neuen Nutzer anlegen"))
        self.pushButton_03.setText(_translate("Dialog_RO_main", "Arbeitsstand auswählen"))
        self.pushButton_02.setText(_translate("Dialog_RO_main", "Bestehenden Nutzer auswählen"))
        self.pushButton_ad_delete.setText(_translate("Dialog_RO_main", "Administration"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog_RO_main = QtWidgets.QDialog()
    ui = Ui_Dialog_RO_main()
    ui.setupUi(Dialog_RO_main)
    Dialog_RO_main.show()
    sys.exit(app.exec_())
