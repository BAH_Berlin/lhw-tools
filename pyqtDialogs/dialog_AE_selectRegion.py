# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_AE_selectRegion.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog_AE_selectRegion(object):
    def setupUi(self, Dialog_AE_selectRegion):
        Dialog_AE_selectRegion.setObjectName("Dialog_AE_selectRegion")
        Dialog_AE_selectRegion.resize(409, 200)
        self.gridLayout_2 = QtWidgets.QGridLayout(Dialog_AE_selectRegion)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label_6 = QtWidgets.QLabel(Dialog_AE_selectRegion)
        self.label_6.setObjectName("label_6")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_6)
        self.lineEditArea = QtWidgets.QLineEdit(Dialog_AE_selectRegion)
        self.lineEditArea.setMinimumSize(QtCore.QSize(0, 24))
        self.lineEditArea.setObjectName("lineEditArea")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.lineEditArea)
        self.label_7 = QtWidgets.QLabel(Dialog_AE_selectRegion)
        self.label_7.setObjectName("label_7")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_7)
        self.lineEditFGWID = QtWidgets.QLineEdit(Dialog_AE_selectRegion)
        self.lineEditFGWID.setMinimumSize(QtCore.QSize(0, 24))
        self.lineEditFGWID.setObjectName("lineEditFGWID")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.lineEditFGWID)
        self.gridLayout.addLayout(self.formLayout, 0, 0, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(Dialog_AE_selectRegion)
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.pushButtonSelect = QtWidgets.QPushButton(Dialog_AE_selectRegion)
        self.pushButtonSelect.setMinimumSize(QtCore.QSize(128, 24))
        self.pushButtonSelect.setObjectName("pushButtonSelect")
        self.horizontalLayout_2.addWidget(self.pushButtonSelect)
        self.gridLayout.addLayout(self.horizontalLayout_2, 1, 0, 1, 1)
        self.line = QtWidgets.QFrame(Dialog_AE_selectRegion)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 2, 0, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_2 = QtWidgets.QLabel(Dialog_AE_selectRegion)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.pushButtonAddArea = QtWidgets.QPushButton(Dialog_AE_selectRegion)
        self.pushButtonAddArea.setMinimumSize(QtCore.QSize(128, 24))
        self.pushButtonAddArea.setObjectName("pushButtonAddArea")
        self.horizontalLayout.addWidget(self.pushButtonAddArea)
        self.gridLayout.addLayout(self.horizontalLayout, 3, 0, 1, 1)
        self.line_3 = QtWidgets.QFrame(Dialog_AE_selectRegion)
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.gridLayout.addWidget(self.line_3, 4, 0, 1, 1)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_3 = QtWidgets.QLabel(Dialog_AE_selectRegion)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem2)
        self.pushButtonSave = QtWidgets.QPushButton(Dialog_AE_selectRegion)
        self.pushButtonSave.setMinimumSize(QtCore.QSize(128, 24))
        self.pushButtonSave.setObjectName("pushButtonSave")
        self.horizontalLayout_3.addWidget(self.pushButtonSave)
        self.gridLayout.addLayout(self.horizontalLayout_3, 5, 0, 1, 1)
        self.line_2 = QtWidgets.QFrame(Dialog_AE_selectRegion)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.gridLayout.addWidget(self.line_2, 6, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_AE_selectRegion)
        QtCore.QMetaObject.connectSlotsByName(Dialog_AE_selectRegion)

    def retranslateUi(self, Dialog_AE_selectRegion):
        _translate = QtCore.QCoreApplication.translate
        Dialog_AE_selectRegion.setWindowTitle(_translate("Dialog_AE_selectRegion", "Dialog"))
        self.label_6.setText(_translate("Dialog_AE_selectRegion", "Name für das Gebiet angeben"))
        self.label_7.setText(_translate("Dialog_AE_selectRegion", "Fließgewässer-ID angeben           "))
        self.label.setText(_translate("Dialog_AE_selectRegion", "Gebietsselektion durchführen"))
        self.pushButtonSelect.setText(_translate("Dialog_AE_selectRegion", "Selektieren"))
        self.label_2.setText(_translate("Dialog_AE_selectRegion", "Gebiet um selektierte Teilgebiete erweitern "))
        self.pushButtonAddArea.setText(_translate("Dialog_AE_selectRegion", "Zuweisen"))
        self.label_3.setText(_translate("Dialog_AE_selectRegion", "Neues Modell erzeugen"))
        self.pushButtonSave.setText(_translate("Dialog_AE_selectRegion", "Speichern"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog_AE_selectRegion = QtWidgets.QDialog()
    ui = Ui_Dialog_AE_selectRegion()
    ui.setupUi(Dialog_AE_selectRegion)
    Dialog_AE_selectRegion.show()
    sys.exit(app.exec_())
