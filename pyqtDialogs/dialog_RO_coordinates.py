# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_RO_coordinates.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog_RO_coordinates(object):
    def setupUi(self, Dialog_RO_coordinates):
        Dialog_RO_coordinates.setObjectName("Dialog_RO_coordinates")
        Dialog_RO_coordinates.resize(261, 106)
        self.gridLayout_2 = QtWidgets.QGridLayout(Dialog_RO_coordinates)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(Dialog_RO_coordinates)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(Dialog_RO_coordinates)
        self.lineEdit.setMinimumSize(QtCore.QSize(0, 24))
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 0, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(Dialog_RO_coordinates)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.lineEdit_2 = QtWidgets.QLineEdit(Dialog_RO_coordinates)
        self.lineEdit_2.setMinimumSize(QtCore.QSize(0, 24))
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.gridLayout.addWidget(self.lineEdit_2, 1, 1, 1, 1)
        self.pushButtonCancel = QtWidgets.QPushButton(Dialog_RO_coordinates)
        self.pushButtonCancel.setMinimumSize(QtCore.QSize(100, 24))
        self.pushButtonCancel.setObjectName("pushButtonCancel")
        self.gridLayout.addWidget(self.pushButtonCancel, 2, 0, 1, 1)
        self.pushButton = QtWidgets.QPushButton(Dialog_RO_coordinates)
        self.pushButton.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 2, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_RO_coordinates)
        QtCore.QMetaObject.connectSlotsByName(Dialog_RO_coordinates)

    def retranslateUi(self, Dialog_RO_coordinates):
        _translate = QtCore.QCoreApplication.translate
        Dialog_RO_coordinates.setWindowTitle(_translate("Dialog_RO_coordinates", "Koordinatenwahl"))
        self.label.setText(_translate("Dialog_RO_coordinates", "RW (LS89 Zone32)"))
        self.label_2.setText(_translate("Dialog_RO_coordinates", "HW (LS89 Zone32)"))
        self.pushButtonCancel.setText(_translate("Dialog_RO_coordinates", "Abbrechen"))
        self.pushButton.setText(_translate("Dialog_RO_coordinates", "Übernehmen"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog_RO_coordinates = QtWidgets.QDialog()
    ui = Ui_Dialog_RO_coordinates()
    ui.setupUi(Dialog_RO_coordinates)
    Dialog_RO_coordinates.show()
    sys.exit(app.exec_())
