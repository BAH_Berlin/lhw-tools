# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_RO_choose_user.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog_choose_perm(object):
    def setupUi(self, Dialog_choose_perm):
        Dialog_choose_perm.setObjectName("Dialog_choose_perm")
        Dialog_choose_perm.resize(911, 373)
        self.gridLayout = QtWidgets.QGridLayout(Dialog_choose_perm)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.pushButton = QtWidgets.QPushButton(Dialog_choose_perm)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout.addWidget(self.pushButton)
        self.gridLayout.addLayout(self.horizontalLayout, 1, 0, 1, 1)
        self.tableWidget = QtWidgets.QTableWidget(Dialog_choose_perm)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.gridLayout.addWidget(self.tableWidget, 0, 0, 1, 1)

        self.retranslateUi(Dialog_choose_perm)
        QtCore.QMetaObject.connectSlotsByName(Dialog_choose_perm)

    def retranslateUi(self, Dialog_choose_perm):
        _translate = QtCore.QCoreApplication.translate
        Dialog_choose_perm.setWindowTitle(_translate("Dialog_choose_perm", "Auswahl eines Nutzers"))
        self.pushButton.setText(_translate("Dialog_choose_perm", "Auswählen"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog_choose_perm = QtWidgets.QDialog()
    ui = Ui_Dialog_choose_perm()
    ui.setupUi(Dialog_choose_perm)
    Dialog_choose_perm.show()
    sys.exit(app.exec_())
