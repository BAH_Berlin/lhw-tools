# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_AE_Main_tab.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog_AE_main(object):
    def setupUi(self, Dialog_AE_main):
        Dialog_AE_main.setObjectName("Dialog_AE_main")
        Dialog_AE_main.resize(371, 162)
        self.gridLayout_6 = QtWidgets.QGridLayout(Dialog_AE_main)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_3 = QtWidgets.QLabel(Dialog_AE_main)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout.addWidget(self.label_3)
        self.pushButtonLoad = QtWidgets.QPushButton(Dialog_AE_main)
        self.pushButtonLoad.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButtonLoad.setObjectName("pushButtonLoad")
        self.horizontalLayout.addWidget(self.pushButtonLoad)
        self.gridLayout_6.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        self.tabWidget = QtWidgets.QTabWidget(Dialog_AE_main)
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.tab)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.tab)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.tab)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 1, 1, 1)
        self.pushButtonLoadArea = QtWidgets.QPushButton(self.tab)
        self.pushButtonLoadArea.setMinimumSize(QtCore.QSize(140, 24))
        self.pushButtonLoadArea.setObjectName("pushButtonLoadArea")
        self.gridLayout.addWidget(self.pushButtonLoadArea, 1, 0, 1, 1)
        self.pushButtonSelectArea = QtWidgets.QPushButton(self.tab)
        self.pushButtonSelectArea.setMinimumSize(QtCore.QSize(140, 24))
        self.pushButtonSelectArea.setObjectName("pushButtonSelectArea")
        self.gridLayout.addWidget(self.pushButtonSelectArea, 1, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.tab_2)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.pushButtonDB = QtWidgets.QPushButton(self.tab_2)
        self.pushButtonDB.setMinimumSize(QtCore.QSize(128, 24))
        self.pushButtonDB.setObjectName("pushButtonDB")
        self.gridLayout_5.addWidget(self.pushButtonDB, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_2, "")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.tab_3)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.pushButtonStartAE = QtWidgets.QPushButton(self.tab_3)
        self.pushButtonStartAE.setMinimumSize(QtCore.QSize(128, 24))
        self.pushButtonStartAE.setObjectName("pushButtonStartAE")
        self.gridLayout_4.addWidget(self.pushButtonStartAE, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_3, "")
        self.tab_4 = QtWidgets.QWidget()
        self.tab_4.setObjectName("tab_4")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.tab_4)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.pushButtonShowResults = QtWidgets.QPushButton(self.tab_4)
        self.pushButtonShowResults.setMinimumSize(QtCore.QSize(128, 24))
        self.pushButtonShowResults.setObjectName("pushButtonShowResults")
        self.gridLayout_3.addWidget(self.pushButtonShowResults, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_4, "")
        self.gridLayout_6.addWidget(self.tabWidget, 1, 0, 1, 1)

        self.retranslateUi(Dialog_AE_main)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(Dialog_AE_main)

    def retranslateUi(self, Dialog_AE_main):
        _translate = QtCore.QCoreApplication.translate
        Dialog_AE_main.setWindowTitle(_translate("Dialog_AE_main", "Untersuchungen mit ArcEGMO"))
        self.label_3.setText(_translate("Dialog_AE_main", "Kartenmaterial Fließgewässer"))
        self.pushButtonLoad.setText(_translate("Dialog_AE_main", "Laden"))
        self.label.setText(_translate("Dialog_AE_main", "Lade ein gepeichertes Gebiet"))
        self.label_2.setText(_translate("Dialog_AE_main", "Selektiere ein Gebiet aus Karte"))
        self.pushButtonLoadArea.setText(_translate("Dialog_AE_main", "Gebiet laden"))
        self.pushButtonSelectArea.setText(_translate("Dialog_AE_main", "Gebiet selektieren"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("Dialog_AE_main", "Gebietsauswahl"))
        self.pushButtonDB.setText(_translate("Dialog_AE_main", "Editieren"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("Dialog_AE_main", "Datenbestand"))
        self.pushButtonStartAE.setText(_translate("Dialog_AE_main", "Durchführen"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), _translate("Dialog_AE_main", "Rechnung"))
        self.pushButtonShowResults.setText(_translate("Dialog_AE_main", "Anzeigen"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4), _translate("Dialog_AE_main", "Ergebnisse"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog_AE_main = QtWidgets.QDialog()
    ui = Ui_Dialog_AE_main()
    ui.setupUi(Dialog_AE_main)
    Dialog_AE_main.show()
    sys.exit(app.exec_())
