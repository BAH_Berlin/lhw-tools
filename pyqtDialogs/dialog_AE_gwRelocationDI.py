# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_AE_gwRelocationDI.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog_AE_gwRelocationDI(object):
    def setupUi(self, Dialog_AE_gwRelocationDI):
        Dialog_AE_gwRelocationDI.setObjectName("Dialog_AE_gwRelocationDI")
        Dialog_AE_gwRelocationDI.resize(303, 211)
        self.gridLayout = QtWidgets.QGridLayout(Dialog_AE_gwRelocationDI)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.Bezeichnung_txt_2 = QtWidgets.QLabel(Dialog_AE_gwRelocationDI)
        font = QtGui.QFont()
        font.setPointSize(9)
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.Bezeichnung_txt_2.setFont(font)
        self.Bezeichnung_txt_2.setObjectName("Bezeichnung_txt_2")
        self.verticalLayout.addWidget(self.Bezeichnung_txt_2)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName("formLayout")
        self.Lagekoordinaten = QtWidgets.QLabel(Dialog_AE_gwRelocationDI)
        self.Lagekoordinaten.setObjectName("Lagekoordinaten")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.Lagekoordinaten)
        self.X_Koordinate = QtWidgets.QLineEdit(Dialog_AE_gwRelocationDI)
        self.X_Koordinate.setObjectName("X_Koordinate")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.X_Koordinate)
        self.Bezugsflaeche_txt = QtWidgets.QLabel(Dialog_AE_gwRelocationDI)
        self.Bezugsflaeche_txt.setObjectName("Bezugsflaeche_txt")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.Bezugsflaeche_txt)
        self.Bezugsflaeche = QtWidgets.QLineEdit(Dialog_AE_gwRelocationDI)
        self.Bezugsflaeche.setObjectName("Bezugsflaeche")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.Bezugsflaeche)
        self.Bezeichnung_txt = QtWidgets.QLabel(Dialog_AE_gwRelocationDI)
        self.Bezeichnung_txt.setObjectName("Bezeichnung_txt")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.Bezeichnung_txt)
        self.spinBox = QtWidgets.QSpinBox(Dialog_AE_gwRelocationDI)
        self.spinBox.setObjectName("spinBox")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.spinBox)
        self.verticalLayout.addLayout(self.formLayout)
        self.line = QtWidgets.QFrame(Dialog_AE_gwRelocationDI)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton = QtWidgets.QPushButton(Dialog_AE_gwRelocationDI)
        self.pushButton.setMinimumSize(QtCore.QSize(128, 0))
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout.addWidget(self.pushButton)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.pushButton_3 = QtWidgets.QPushButton(Dialog_AE_gwRelocationDI)
        self.pushButton_3.setMinimumSize(QtCore.QSize(128, 0))
        self.pushButton_3.setObjectName("pushButton_3")
        self.horizontalLayout.addWidget(self.pushButton_3)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_AE_gwRelocationDI)
        QtCore.QMetaObject.connectSlotsByName(Dialog_AE_gwRelocationDI)

    def retranslateUi(self, Dialog_AE_gwRelocationDI):
        _translate = QtCore.QCoreApplication.translate
        Dialog_AE_gwRelocationDI.setWindowTitle(_translate("Dialog_AE_gwRelocationDI", "Dialog"))
        self.Bezeichnung_txt_2.setText(_translate("Dialog_AE_gwRelocationDI", "Grundwasserverlagerung"))
        self.Lagekoordinaten.setText(_translate("Dialog_AE_gwRelocationDI", "TG-ID Ursprungsgebiet"))
        self.Bezugsflaeche_txt.setText(_translate("Dialog_AE_gwRelocationDI", "TG-ID Zielgebiet"))
        self.Bezeichnung_txt.setText(_translate("Dialog_AE_gwRelocationDI", "Anteil der GW-Verlagerung [%]"))
        self.pushButton.setText(_translate("Dialog_AE_gwRelocationDI", "Abbrechen"))
        self.pushButton_3.setText(_translate("Dialog_AE_gwRelocationDI", "Übernehmen"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog_AE_gwRelocationDI = QtWidgets.QDialog()
    ui = Ui_Dialog_AE_gwRelocationDI()
    ui.setupUi(Dialog_AE_gwRelocationDI)
    Dialog_AE_gwRelocationDI.show()
    sys.exit(app.exec_())
