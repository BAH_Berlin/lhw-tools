# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_AD_delete.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog_admin_delete(object):
    def setupUi(self, Dialog_admin_delete):
        Dialog_admin_delete.setObjectName("Dialog_admin_delete")
        Dialog_admin_delete.resize(168, 69)
        self.gridLayout_2 = QtWidgets.QGridLayout(Dialog_admin_delete)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(Dialog_admin_delete)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.comboBox_ids = QtWidgets.QComboBox(Dialog_admin_delete)
        self.comboBox_ids.setObjectName("comboBox_ids")
        self.gridLayout.addWidget(self.comboBox_ids, 0, 1, 1, 1)
        self.pushButton_delete = QtWidgets.QPushButton(Dialog_admin_delete)
        self.pushButton_delete.setObjectName("pushButton_delete")
        self.gridLayout.addWidget(self.pushButton_delete, 1, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_admin_delete)
        QtCore.QMetaObject.connectSlotsByName(Dialog_admin_delete)

    def retranslateUi(self, Dialog_admin_delete):
        _translate = QtCore.QCoreApplication.translate
        Dialog_admin_delete.setWindowTitle(_translate("Dialog_admin_delete", "Dialog"))
        self.label.setText(_translate("Dialog_admin_delete", "ID in wnv_lhw"))
        self.pushButton_delete.setText(_translate("Dialog_admin_delete", "Löschen"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog_admin_delete = QtWidgets.QDialog()
    ui = Ui_Dialog_admin_delete()
    ui.setupUi(Dialog_admin_delete)
    Dialog_admin_delete.show()
    sys.exit(app.exec_())
