# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_WB_main.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog_WB_main(object):
    def setupUi(self, Dialog_WB_main):
        Dialog_WB_main.setObjectName("Dialog_WB_main")
        Dialog_WB_main.resize(406, 463)
        self.gridLayout_2 = QtWidgets.QGridLayout(Dialog_WB_main)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(Dialog_WB_main)
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName("formLayout")
        self.label_2 = QtWidgets.QLabel(Dialog_WB_main)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.label_3 = QtWidgets.QLabel(Dialog_WB_main)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.pushButton_01 = QtWidgets.QPushButton(Dialog_WB_main)
        self.pushButton_01.setObjectName("pushButton_01")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.pushButton_01)
        self.label_activeLayer = QtWidgets.QLabel(Dialog_WB_main)
        self.label_activeLayer.setMinimumSize(QtCore.QSize(0, 18))
        self.label_activeLayer.setObjectName("label_activeLayer")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.label_activeLayer)
        self.gridLayout.addLayout(self.formLayout, 1, 0, 1, 1)
        self.line = QtWidgets.QFrame(Dialog_WB_main)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 2, 0, 1, 1)
        self.label_4 = QtWidgets.QLabel(Dialog_WB_main)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)
        self.textEdit = QtWidgets.QTextEdit(Dialog_WB_main)
        self.textEdit.setObjectName("textEdit")
        self.gridLayout.addWidget(self.textEdit, 4, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_WB_main)
        QtCore.QMetaObject.connectSlotsByName(Dialog_WB_main)

    def retranslateUi(self, Dialog_WB_main):
        _translate = QtCore.QCoreApplication.translate
        Dialog_WB_main.setWindowTitle(_translate("Dialog_WB_main", "Wasserhaushalt"))
        self.label.setText(_translate("Dialog_WB_main", "Wasserhaushalt"))
        self.label_2.setText(_translate("Dialog_WB_main", "Aktiver Layer ist:    "))
        self.label_3.setText(_translate("Dialog_WB_main", "Unterirdische Abflusskomponenten auswählen  "))
        self.pushButton_01.setText(_translate("Dialog_WB_main", "Auswahl"))
        self.label_activeLayer.setText(_translate("Dialog_WB_main", "Aktiver Layer"))
        self.label_4.setText(_translate("Dialog_WB_main", "Flächengemittelte Grundwasserneubildung nach ausgewählten Komponenten"))
        self.textEdit.setHtml(_translate("Dialog_WB_main", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Komponente      [mm/a]</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">---------------------------</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">RG2                     100</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">RG1                     100</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Rdrain                  50</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">==============</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">GWN                    250</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">==============</span></p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog_WB_main = QtWidgets.QDialog()
    ui = Ui_Dialog_WB_main()
    ui.setupUi(Dialog_WB_main)
    Dialog_WB_main.show()
    sys.exit(app.exec_())
