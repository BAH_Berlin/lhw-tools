# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_RO_newReference.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog_RO_newReference(object):
    def setupUi(self, Dialog_RO_newReference):
        Dialog_RO_newReference.setObjectName("Dialog_RO_newReference")
        Dialog_RO_newReference.resize(264, 103)
        self.gridLayout = QtWidgets.QGridLayout(Dialog_RO_newReference)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.label = QtWidgets.QLabel(Dialog_RO_newReference)
        self.label.setMinimumSize(QtCore.QSize(0, 24))
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(Dialog_RO_newReference)
        self.lineEdit.setMinimumSize(QtCore.QSize(0, 24))
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout_2.addWidget(self.lineEdit, 1, 0, 1, 1)
        self.pushButton = QtWidgets.QPushButton(Dialog_RO_newReference)
        self.pushButton.setMinimumSize(QtCore.QSize(24, 0))
        self.pushButton.setObjectName("pushButton")
        self.gridLayout_2.addWidget(self.pushButton, 2, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 0, 0, 1, 1)

        self.retranslateUi(Dialog_RO_newReference)
        QtCore.QMetaObject.connectSlotsByName(Dialog_RO_newReference)

    def retranslateUi(self, Dialog_RO_newReference):
        _translate = QtCore.QCoreApplication.translate
        Dialog_RO_newReference.setWindowTitle(_translate("Dialog_RO_newReference", "Dialog"))
        self.label.setText(_translate("Dialog_RO_newReference", "Neues Aktenzeichen für \"\" eingeben:"))
        self.pushButton.setText(_translate("Dialog_RO_newReference", "Übernehmen"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog_RO_newReference = QtWidgets.QDialog()
    ui = Ui_Dialog_RO_newReference()
    ui.setupUi(Dialog_RO_newReference)
    Dialog_RO_newReference.show()
    sys.exit(app.exec_())
