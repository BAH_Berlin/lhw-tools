# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_RO_selectExtraction.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog_RO_selectExtraction(object):
    def setupUi(self, Dialog_RO_selectExtraction):
        Dialog_RO_selectExtraction.setObjectName("Dialog_RO_selectExtraction")
        Dialog_RO_selectExtraction.resize(1091, 613)
        self.gridLayout_3 = QtWidgets.QGridLayout(Dialog_RO_selectExtraction)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.scrollArea = QtWidgets.QScrollArea(Dialog_RO_selectExtraction)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 1071, 593))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.scrollAreaWidgetContents)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setContentsMargins(5, 5, 5, 5)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.pushButton = QtWidgets.QPushButton(self.scrollAreaWidgetContents)
        self.pushButton.setMinimumSize(QtCore.QSize(200, 24))
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_2.addWidget(self.pushButton)
        self.gridLayout.addLayout(self.horizontalLayout_2, 3, 0, 1, 1)
        self.tableWidget = QtWidgets.QTableWidget(self.scrollAreaWidgetContents)
        self.tableWidget.setMinimumSize(QtCore.QSize(310, 400))
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.gridLayout.addWidget(self.tableWidget, 2, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.gridLayout_3.addWidget(self.scrollArea, 0, 0, 1, 1)

        self.retranslateUi(Dialog_RO_selectExtraction)
        QtCore.QMetaObject.connectSlotsByName(Dialog_RO_selectExtraction)

    def retranslateUi(self, Dialog_RO_selectExtraction):
        _translate = QtCore.QCoreApplication.translate
        Dialog_RO_selectExtraction.setWindowTitle(_translate("Dialog_RO_selectExtraction", "Dialog"))
        self.label.setText(_translate("Dialog_RO_selectExtraction", "Auswahl von Entnahmen oder Einleitungen"))
        self.label_2.setText(_translate("Dialog_RO_selectExtraction", "(Akzeptieren einer Auswahl mit j oder 1, Ablehnen mit n oder 0)"))
        self.pushButton.setText(_translate("Dialog_RO_selectExtraction", "Übernehmen"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog_RO_selectExtraction = QtWidgets.QDialog()
    ui = Ui_Dialog_RO_selectExtraction()
    ui.setupUi(Dialog_RO_selectExtraction)
    Dialog_RO_selectExtraction.show()
    sys.exit(app.exec_())
