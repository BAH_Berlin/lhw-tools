# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_RO_extractionConst.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog_RO_extractionConst(object):
    def setupUi(self, Dialog_RO_extractionConst):
        Dialog_RO_extractionConst.setObjectName("Dialog_RO_extractionConst")
        Dialog_RO_extractionConst.resize(258, 108)
        self.gridLayout_2 = QtWidgets.QGridLayout(Dialog_RO_extractionConst)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(Dialog_RO_extractionConst)
        self.label.setMinimumSize(QtCore.QSize(0, 24))
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(Dialog_RO_extractionConst)
        self.lineEdit.setMinimumSize(QtCore.QSize(0, 24))
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 1, 0, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButtonReject = QtWidgets.QPushButton(Dialog_RO_extractionConst)
        self.pushButtonReject.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButtonReject.setObjectName("pushButtonReject")
        self.horizontalLayout.addWidget(self.pushButtonReject)
        self.pushButtonAccept = QtWidgets.QPushButton(Dialog_RO_extractionConst)
        self.pushButtonAccept.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButtonAccept.setObjectName("pushButtonAccept")
        self.horizontalLayout.addWidget(self.pushButtonAccept)
        self.gridLayout.addLayout(self.horizontalLayout, 2, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_RO_extractionConst)
        QtCore.QMetaObject.connectSlotsByName(Dialog_RO_extractionConst)

    def retranslateUi(self, Dialog_RO_extractionConst):
        _translate = QtCore.QCoreApplication.translate
        Dialog_RO_extractionConst.setWindowTitle(_translate("Dialog_RO_extractionConst", "Dialog"))
        self.label.setText(_translate("Dialog_RO_extractionConst", "Jährlich konstante Entnahme [Tm³/Jahr]"))
        self.lineEdit.setText(_translate("Dialog_RO_extractionConst", "0"))
        self.pushButtonReject.setText(_translate("Dialog_RO_extractionConst", "Verwerfen"))
        self.pushButtonAccept.setText(_translate("Dialog_RO_extractionConst", "Übernehmen"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog_RO_extractionConst = QtWidgets.QDialog()
    ui = Ui_Dialog_RO_extractionConst()
    ui.setupUi(Dialog_RO_extractionConst)
    Dialog_RO_extractionConst.show()
    sys.exit(app.exec_())
