# LHW_Tools

![MODLFOW-Pywr](docs/source/_static/figure_01.png)

## Version
Version 2.16  
This Software is designed as a plug-in for QGIS >3.16.

## Introduction
LHW_Tools is a Plugin for QGIS3 (3.16). It was financed and written for the Landesbetrieb für Hochwasserschutz und Wasserwirtschaft Sachsen-Anhalt (LHW Sachsen-Anhalt).
The LHW_Toolbox assists in making statements about requested groundwater extractions in the state of Saxony-Anhalt.

The main feature of the toolbox is a iterative approximation of the draw-down cones of extraction wells based on spatially distributed groundwater recharges and groundwater level data.
This software will requires a suitable database and shape files to work correctly (see <to be added>).

## Author
Ruben Müller  
E-Mail: ruben.mueller@bah-berlin.de  
Company: Büro für Angewandte Hydrologie, Berlin  
web: www.bah-berlin.de  

## Conception  
Ruben Müller 1, Bernd Pfützner 1, Petra Hesse 1, Silke Mey 1,  
Martin Schneppmüller 2, Uta Rehn 2, Angelika Huber 2, Karin Gruschinski 2, Franziska Halbing 2, Ulrike Leifholz 2  

1 Büro für Angewandte Hydrologie Berlin, 2 LHW Sachsen-Anhalt

## Documentation
A manual in German language is provided in the docs folder.

## License
Copyright (C) 2020 Ruben Müller, Büro für Angewandte Hydrologie, Berlin  

This program is free software; you can redistribute it and/or modify  
it under the terms of the GNU General Public License as published by  
the Free Software Foundation; either version 1, or (at your option)  
any later version.

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License  
along with this program; if not, write to the Free Software  
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA.