# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Provides some functionality to access the local data base.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

import os
import psycopg2
import psycopg2.extras

from qgis.core import Qgis
from qgis.utils import iface
from qgis.core import QgsDataSourceUri
from qgis.core import QgsVectorLayer
from qgis.core import QgsProject
from qgis.core import NULL

from .qgis_toolbox import check_layer_loaded

###
### for debug
###
if 1:
    from ..setup import setup__ as SETUP
else:
    class setup:
        def __init__(self):
            ### database to read only
            self.HOST_RO = '127.0.0.1'
            self.PORT_RO = '5432'
            self.DB_RO = 'testLHW'
            self.TABLE_RO = 'nutzer'
            ### database to write
            self.HOST_RW = '127.0.0.1'
            self.PORT_RW = '5432'
            self.DB_RW = 'testLHW'
            self.TABLEW_RW = 'wellCatchment'
            self.TABLEB_RW = 'balanceCatchment'
            self.USERMAP = {'default': 'postgres',
                                       'Ruben.Mueller': 'postgres'}
    SETUP = setup()


class userDB:
    '''class handles a postgis connection.
    
    Parameters
    ----------
    dbytype : str
        rw for the local read-write db OR ro for the network read only db'''
    def __init__(self, dbtype):
        if dbtype.lower() == 'ro':
            self.dbwrite = False
        elif dbtype.lower() == 'rw':
            self.dbwrite = True
        else :
            iface.messageBar().pushMessage(u'wrong DB type. Set read only.',
                                           level=Qgis.Error,
                                            duration=7)
            self.dbtype = 0
        self.connection = False


    def __del__(self):
        '''try to disconnect from data base when instantion is killed'''
        try:
            self.connection.close()
        except:
            pass


    def close(self):
        '''close a connection to the data base'''
        self.connection.close()


    def commit(self):
        '''execute a commit'''
        self.connection.commit()


    def make_connection(self):
        '''connect to the data base'''
        if not self.connection:
            self.connection = psycopg2.connect(**self.get_dbparams())


    def get_DB_user(self):
        '''get the windows log in and return the assigned postgres user.
        return the default read only postgres user windows user in is not found'''
        winUser = os.getenv('username')
        #print(winUser, SETUP.USERMAP)
        #winUser = os.getlogin() --> not in standard qgis 2.18
        if not winUser in SETUP.USERMAP:
            tmpstr = u'Nutzer {} im nicht gefunden. Melde nur mit leserechten an.'
            iface.messageBar().pushMessage(tmpstr.format(winUser),
                                           level=Qgis.Warning, duration=3)
            puser = SETUP.USERMAP['default']
        else:
            puser = SETUP.USERMAP[winUser]
        return puser       


    def get_dbparams(self):
        '''return the parameters for the data base connection from the setup'''
        user = self.get_DB_user()
        db_params = {
            'dbname': SETUP.DB_RW,
            'user': user[0],
            'host': SETUP.HOST_RW,
            'password': user[1],
            'port': SETUP.PORT_RW
        }
        return db_params


    def execute_sql(self, sql, dict_cursor=False, print_sql=False, close=True, commit=True):
        """ Execute a SQL query
        
        Parameters
        ----------
        sql : str
            SQL to be executed
        dict_cursor: bool
            Flag indicating if cursor is a dict or not. Use false for scalar queries
        print_sql : bool
            Flag indicating if sql is to be printed
        close : bool
            Flag indicating if the connection is to closed afterwards
        commit : bool
            Flag indicating if a commit is to be done or if we exit pending
            
        Returns
        -------
        cur : cursor object
            the cursor with the results"""
        if print_sql:
            print(sql)
        self.make_connection()

        if dict_cursor:
            cur = self.connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
        else:
            cur = self.connection.cursor()
        try:
            if isinstance(sql, list):
                cur.execute(sql[0], sql[1])
            else:
                cur.execute(sql) 
            
        except psycopg2.DatabaseError as e:
            iface.messageBar().pushMessage(u'Fehlgeschlagen: {} for {}'.format(e, sql),
                                           level=Qgis.Warning, duration=7)
        finally:
            if self.connection:
                if commit:
                    self.connection.commit()
                if close:
                    self.connection.close()
            return cur

    def update_single_entry(self, inDict, dict_cursor=False,
                            print_sql=False, close=False, commit=False):
        """ Execute a SQL query to update a single field
        
        Parameters
        ----------
        inDict : dict
            contains the infomation to assamble the SQL string to be executed
        dict_cursor: bool
            Flag indicating if cursor is a dict or not. Use false for scalar queries
        print_sql : bool
            Flag indicating if sql is to be printed
        close : bool
            Flag indicating if the connection is to closed afterwards
        commit : bool
            Flag indicating if a commit is to be done or if we exit pending"""
        for key in inDict:
            if not inDict[key] in (NULL, 'NULL')\
            or key in ('GWR flex'):
                if not key in ('cid', 'idx', 'table'):
                    sql = ['UPDATE '+ SETUP.DB_CONF['name'] + '.' + inDict['table'] +' SET "'+ key \
                           + '" = (%s) WHERE ' + inDict['idx'] + ' = (%s)'
                           , (inDict[key], inDict['cid'])]
                    self.execute_sql(sql, close=close, commit=False, 
                                            print_sql=print_sql)
        if commit:
            self.commit()


    def get_last_id(self, tableDict, close=False):
        """ Execute a SQL query to update a single field
        
        Parameters
        ----------
        tableDict : dict
            contains the infomation to assamble the SQL string to be executed
        close : bool
            Flag indicating if the connection is to closed afterwards
            
        Returns
        -------
        lastid : int
            the highest id in the table"""
        tab = tableDict['table']
        sql = "SELECT max("+ SETUP.DB_CONF[tab]['id'] +") FROM " \
              + SETUP.DB_CONF['name'] + '.' + tableDict['table']
        #print(sql)
        curser = self.execute_sql(sql, close=close, commit=False)
        lastid = curser.fetchall()[0][0]
        #curser.close()  ### don't do that! why? not commited yet?
        return lastid
    
    
    
    def check_for_az(self, tableDict, close=False):
        """ Execute a SQL query to check if the az is already in the table.
        
        Parameters
        ----------
        tableDict : dict
            contains the infomation to assamble the SQL string to be executed
        close : bool
            Flag indicating if the connection is to closed afterwards
            
        Reurns
        ------
        tmp : bool
            True if az is already in the table"""            
        tab = tableDict['table']
        sql = "SELECT id FROM " \
              + SETUP.DB_CONF['name'] + '.' + tableDict['table'] \
              + " WHERE " + SETUP.DB_CONF[tab]['az'] + " = '{}'".format(tableDict['az'])
        curser = self.execute_sql(sql, close=close, commit=False)
        try:
            a = curser.fetchall()[0][0]
            return a
        except:
            return None


    def check_for_id(self, tableDict, close=False):
        """ Execute a SQL query to check if the id is already in the table.
        
        Parameters
        ----------
        tableDict : dict
            contains the infomation to assamble the SQL string to be executed
        close : bool
            Flag indicating if the connection is to closed afterwards
            
        Reurns
        ------
        tmp : bool
            True if id is already in the table"""
        #tab = tableDict['table']
        sql = "SELECT id FROM " \
              + SETUP.DB_CONF['name'] + '.' + tableDict['table'] \
              + " WHERE id = '{}'".format(tableDict['id'])
        curser = self.execute_sql(sql, close=close, commit=False)
        try:
            a = curser.fetchall()[0][0]
            return a
        except:
            return None


    def check_for_coordinates_base(self, tableDict, close=False):
        """ Execute a SQL query to check if there is another well in a vary close proximity.
        
        Parameters
        ----------
        tableDict : dict
            contains the infomation to assamble the SQL string to be executed
        close : bool
            Flag indicating if the connection is to closed afterwards
            
        Reurns
        ------
        tmp : bool
            True if id is already in the table"""
        tab = tableDict['table']
        xhigh = str(tableDict['x'] + SETUP.WELL_PROXIMITY)
        xlow = str(tableDict['x'] - SETUP.WELL_PROXIMITY)
        yhigh = str(tableDict['y'] + SETUP.WELL_PROXIMITY)
        ylow = str(tableDict['y'] - SETUP.WELL_PROXIMITY)
        xstr = SETUP.DB_CONF[tab]['coordinateX']
        ystr = SETUP.DB_CONF[tab]['coordinateY']
        
        sql = "SELECT " + SETUP.DB_CONF[tab]['id'] + " FROM " \
              + SETUP.DB_CONF['name'] + '.' + tableDict['table'] \
              + " WHERE (" +  xstr + " BETWEEN " + xlow + ' AND ' + xhigh \
              + ") AND (" + ystr + " BETWEEN " + ylow + ' AND ' + yhigh + ");"
        #print(sql)
        curser = self.execute_sql(sql, close=close, commit=False)
        try:
            a = curser.fetchall()[0]
            return a
        except:
            return None


    def insert_wellpoint(self, inDict, print_sql=False, 
                         close=False, commit=False, idp=None):
        """ Execute a SQL queryto insert a new wellpoint / user into the data base
        
        Parameters
        ----------
        inDict : dict
            contains the infomation to assamble the SQL string to be executed
        print_sql : bool
            Flag indicating if sql is to be printed
        close : bool
            Flag indicating if the connection is to closed afterwards
        commit : bool
            Flag indicating if a commit is to be done or if we exit pending
            
        Returns
        -------
        idp : int
            the id of the new entry"""
        if idp is None:
            idp = self.get_last_id(inDict) + 1
        tab = inDict['table']
        sql = ['INSERT INTO '+ SETUP.DB_CONF['name'] + '.' + inDict['table'] \
               + '(' + SETUP.DB_CONF[tab]['geom'] + ', ' + SETUP.DB_CONF[tab]['id'] + ') ' \
               + 'VALUES (ST_SetSRID(ST_MakePoint((%s), (%s)), '\
               + str(SETUP.CRS) +'), (%s));'
               , (inDict['x'], inDict['y'], idp)]
        self.execute_sql(sql, close=close, commit=commit)
        return idp

    
    def update_wellpoint(self, inDict, print_sql=False, 
                         close=False, commit=False):
        """ Execute a SQL queryto insert a new wellpoint / user into the data base
        
        Parameters
        ----------
        inDict : dict
            contains the infomation to assamble the SQL string to be executed
        print_sql : bool
            Flag indicating if sql is to be printed
        close : bool
            Flag indicating if the connection is to closed afterwards
        commit : bool
            Flag indicating if a commit is to be done or if we exit pending
            
        Returns
        -------
        idp : int
            the id of the new entry"""
        tab = inDict['table']
        key = SETUP.DB_CONF[tab]['geom']
        sql = ['UPDATE '+ SETUP.DB_CONF['name'] + '.' + inDict['table'] +' SET "'+ key \
               + '" = (ST_SetSRID(ST_MakePoint((%s), (%s)), ' + str(SETUP.CRS) \
               +')) WHERE ' + inDict['idx'] + ' = (%s)'
              , (inDict['x'], inDict['y'], inDict['cid'])]
        if print_sql:
            print(sql)
        self.execute_sql(sql, close=close, commit=commit)
    

    def insert_extrpoint(self, inDict, print_sql=False, 
                         close=False, commit=False, idp=None):
        """ Execute a SQL queryto insert a new extraction point into the data base
        
        Parameters
        ----------
        inDict : dict
            contains the infomation to assamble the SQL string to be executed
        print_sql : bool
            Flag indicating if sql is to be printed
        close : bool
            Flag indicating if the connection is to closed afterwards
        commit : bool
            Flag indicating if a commit is to be done or if we exit pending
            
        Returns
        -------
        idp : int
            the id of the new entry"""
        if idp is None:
            idp = self.get_last_id(inDict) + 1
        tab = inDict['table']
        sql = ['INSERT INTO '+ SETUP.DB_CONF['name'] + '.' + inDict['table'] \
               + '(' + SETUP.DB_CONF[tab]['id'] + ', id)' \
               + ' VALUES ((%s), (%s));'
               , (inDict['id_wnv_lhw'], idp)]
        if print_sql:
            print(sql)
        self.execute_sql(sql, close=close, commit=commit)
        return idp
        

    def insert_catchment(self, inDict, print_sql=False, 
                         close=False, commit=False, idp=None):
        """ Execute a SQL queryto insert a new well catchment into the data base
        
        Parameters
        ----------
        inDict : dict
            contains the infomation to assamble the SQL string to be executed
        print_sql : bool
            Flag indicating if sql is to be printed
        close : bool
            Flag indicating if the connection is to closed afterwards
        commit : bool
            Flag indicating if a commit is to be done or if we exit pending
            
        Returns
        -------
        idp : int
            the id of the new entry"""
        if idp is None:
            idp = self.get_last_id(inDict) + 1
        tab = inDict['table']
        sql = ['INSERT INTO '+ SETUP.DB_CONF['name'] + '.' + inDict['table'] \
               + ' (' + SETUP.DB_CONF[tab]['geom'] + ', ' + SETUP.DB_CONF[tab]['id'] + ', id)' \
               + ' VALUES (ST_SetSRID(ST_GeomFromEWKT((%s)), '\
               + str(SETUP.CRS) +'), (%s), (%s));'
               , (inDict['polgonWKT'], inDict['id_wnv_lhw'], idp)] 
        if print_sql:
            print(sql)
        self.execute_sql(sql, close=close, commit=commit)
        return idp


    def update_catchment(self, inDict, print_sql=False, 
                         close=False, commit=False):
        """ Execute a SQL queryto insert a new well catchment into the data base
        
        Parameters
        ----------
        inDict : dict
            contains the infomation to assamble the SQL string to be executed
        print_sql : bool
            Flag indicating if sql is to be printed
        close : bool
            Flag indicating if the connection is to closed afterwards
        commit : bool
            Flag indicating if a commit is to be done or if we exit pending
            
        Returns
        -------
        idp : int
            the id of the new entry"""
        tab = inDict['table']
        key = SETUP.DB_CONF[tab]['geom']
 
        sql = ['UPDATE '+ SETUP.DB_CONF['name'] + '.' + inDict['table'] +' SET "'+ key \
               + '" = (ST_SetSRID(ST_GeomFromEWKT((%s)), '+ str(SETUP.CRS) \
               + ')) WHERE ' + inDict['idx'] + ' = (%s)'
              , (inDict['polgonWKT'], inDict['cid'])] 
        if print_sql:
            print(sql)
        self.execute_sql(sql, close=close, commit=commit)


    def table_to_layer(self, table, name, geometry=None, idField=None):
        """ load an table from the data base and show it as a layer in the map
        
        Parameters
        ----------
        table : str
            name of the layer to load / show
        name : str
            name under which the layer is registerd
                    
        Returns
        -------
        idp : layer object
            the loaded layer"""
        if not check_layer_loaded(table):
            uri = QgsDataSourceUri()
            settings = self.get_dbparams()
            uri.setConnection(settings['host'], settings['port'], 
                              settings['dbname'], settings['user'], 
                              settings['password'])
            geometry = SETUP.DB_CONF[table]['geom']
            idField = SETUP.DB_CONF[table]['id']
            uri.setDataSource(SETUP.DB_CONF['name'], table, geometry, "", idField)
            
            vlayer = QgsVectorLayer(uri.uri(), name,  "postgres")
            QgsProject.instance().addMapLayer(vlayer, True)
            iface.mapCanvas().refresh()
        else:
            vlayer = QgsProject.instance().mapLayersByName(table)[0]
        return vlayer

    
    def delete_single_entry(self, inDict, dict_cursor=False,
                            print_sql=False, close=False, commit=False):
        """ Execute a SQL query to update a single field
        
        Parameters
        ----------
        inDict : dict
            contains the infomation to assamble the SQL string to be executed
        dict_cursor: bool
            Flag indicating if cursor is a dict or not. Use false for scalar queries
        print_sql : bool
            Flag indicating if sql is to be printed
        close : bool
            Flag indicating if the connection is to closed afterwards
        commit : bool
            Flag indicating if a commit is to be done or if we exit pending"""

        sql = ['DELETE FROM '+ SETUP.DB_CONF['name'] + '.' + inDict['table']
               + ' WHERE ' + SETUP.DB_CONF[inDict['table']]['id'] + ' = (%s)'
               , inDict['cid']]
        self.execute_sql(sql, close=close, commit=False, 
                                print_sql=print_sql)
        if commit:
            self.commit()


    def check_for_coordinates(self, userDict, tablename, init=False, close=False):
        """ wrapper for check_for_coordinates_base
        
        Parameters
        ----------
        userDict : dict
            self.userDict of Start_Dialog_RO_userManagement
        tablename: str
            the table name
        init : bool
            use the initial coordinates or the potentionally changed ones
        close : bool
            Flag indicating if the connection is to closed afterwards"""
        if init:
            x, y = 'coordinateInitX', 'coordinateInitY'
        else:
            x, y = 'coordinateX', 'coordinateY'
 
        inDict = {'x': userDict[x], 'y': userDict[y],'table': tablename}

        id_prox_lhw = self.check_for_coordinates_base(inDict, close=False)

        if isinstance(id_prox_lhw, (list, tuple)):
                id_prox_lhwList = id_prox_lhw
                id_prox_lhw = max(id_prox_lhw)
        else:
                id_prox_lhwList = id_prox_lhw
                id_prox_lhw = id_prox_lhw
        return id_prox_lhw, id_prox_lhwList


if __name__ is '__main__':
    print('hi there.')