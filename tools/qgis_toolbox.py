# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Provides some QGIS functionality 

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

import os
import sys
import pickle
import gzip
import csv
import re
import datetime

from qgis.core import QgsProject
from qgis.PyQt.QtCore import QVariant
from qgis.core import QgsVectorFileWriter
from qgis.core import QgsFeature
from qgis.core import QgsVectorLayer
from qgis.utils import iface
from qgis.core import QgsWkbTypes
from qgis.core import QgsFeatureRequest
from qgis.core import QgsField
from qgis.core import edit
from qgis.core import Qgis

from .. setup import setup__ as SETUP
from qgis.PyQt.QtWidgets import (QMessageBox)


def infoBox(message):
    """Show a questionbox on exiting, asking for permission."""
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setInformativeText(message)
    msg.setWindowTitle("Abweichender KBS")
    msg.setStandardButtons(QMessageBox.Ok)
    msg.exec_()


def check_crs(clayer):
    if isinstance(clayer, str):
        clayer = QgsProject.instance().mapLayersByName('ezg')[0]

    curr_crs = iface.mapCanvas().mapSettings().destinationCrs().authid()
    lay_crs = clayer.crs().authid()
    
    ok = False
    if not curr_crs == lay_crs:
        ok = True
        message = """KBS {} entspricht nicht dem KBS {} des Projekts.
        
Layer wird nicht verwendet!

(In Layerübersicht mit passendem KBS neu speichern.)""".format(lay_crs, curr_crs)
        infoBox(message)
    return ok


def max_catchmend_num():
    """Return the next highest number counting the catchment layers in the canvas."""
    a = 'Brunneneinzugsgebiet'
    mapCanvas = iface.mapCanvas()
    layers = mapCanvas.layers()
    nums = []
    for layer in layers:
        if layer.name()[:20] == a:
            print(layer.name()[20])
            try:
                nums.append(int(layer.name()[20:21]))
            except:
                nums.append(int(layer.name()[20]))
    if nums:
        return max(nums)+1
    else:
        return 1


def check_fieldname(clayer, fieldname, ftype='double', 
                    overwrite=None, new_default=None, debug=False):
    """Check if a field name exists in a layer, create it if not.
    
    Parameters
    ----------
    clayer : layer
        point of vector layer to check
    fieldname : str
        check for this field name
    ftype : str
        type of the field: double, int or str
    debug : bool
        print debug stuff
    
    Returns
    -------
    idx : int
        the column index of the field
    """
    if ftype.lower() in ('double', 'float'):
        fts = QVariant.Double
    elif ftype.lower() in ('int', 'integer'):
        fts = QVariant.Int
    elif ftype.lower() in ('str', 'string'):
        fts = QVariant.String
    else:
        raise ValueError("Wrong argument for ftype. 'double', 'int' or 'str'")
        
    if overwrite and new_default:
        raise ValueError("Please set only one of the *_default arguments")

    clayer.startEditing()
    idx0 = clayer.fields().lookupField(fieldname) 
    if idx0 == -1:
        if debug: print('field not existing {}'.format(fieldname))
        clayer.dataProvider().addAttributes([QgsField(fieldname, fts)])
        clayer.updateFields()
        clayer.commitChanges()
        if new_default:
            clayer.startEditing()
            idx = clayer.fields().lookupField(fieldname)
            for feat in clayer.getFeatures():
                clayer.changeAttributeValue(feat.id(), idx, new_default)
            clayer.commitChanges()

    idx1 = clayer.fields().lookupField(fieldname)
    if overwrite:
        clayer.startEditing()
        for feat in clayer.getFeatures():
            clayer.changeAttributeValue(feat.id(), idx1, overwrite)
        clayer.commitChanges()

    return idx1



def get_layer(layername):
    """Return the layer object belonging to the layername.
    
    Parameters
    ----------
    layername : str
        the name of the layer
    """
    mapCanvas = iface.mapCanvas()
    layers = mapCanvas.layers()
    layer_layername = None
    for layer in layers:
        if layer.name() == layername:
           layer_layername = layer
    if layer_layername is None:
        iface.messageBar().pushMessage(u"Layer {} nicht verfügbar. Deaktiviert?".format(layername),
                                       level=Qgis.Warning, duration=3)
    else:
        return layer_layername


def well_within_TG(geom):
    """Check if the bufferGeometry intersects the catchment.
    
    Parameters
    ----------
    bufferGeometry : buffer geometry object
        the buffer around the well
        
    Returns
    -------
    tmp : bool
        true if bufferGeometry is within the area, false otherwise
    """ 
    layer = get_layer(SETUP.TGLAYER)
    feats = layer.getFeatures()
    idTG = layer.fields().lookupField( "TGID" )
    for polyFeat_inner in feats:
        polyGeom_inner = polyFeat_inner.geometry()
        if geom.within(polyGeom_inner):
            return polyFeat_inner[idTG]


def checkForFloat(strin):
    """Return True if sting is a valid float."""
    pattern = re.compile("^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$")
    chk = True if pattern.match(strin) else False
    return chk


def write_loaded_layer(layerName, folderx, destName=None):
    """Save a registered layer to the ESRI Shapefile.

    Parameters
    ----------
    layerName : str
        name of layer
    folderx : str
        path to folder to save the layer in
    destName : str, optional
        if provided, set this as the new layer name
    """
    if not layerName == '(nicht gesetzt)':
        try:
            if destName is None:
                destName = layerName
            wellc = os.path.join(folderx, destName)
            if check_layer_loaded(layerName):
                layer = QgsProject.instance().mapLayersByName(layerName)[0]
                QgsVectorFileWriter.writeAsVectorFormat(layer, wellc, "UTF-8", 
                    destCRS=layer.crs(), driverName="ESRI Shapefile")
            else:
                iface.messageBar().pushMessage(u"Layer " + layerName \
                      + " nicht gefunden. KEIN Shape des Brunneneinzugsgebiets gespeichert!",
                                               level=Qgis.Warning, duration=7)
        except Exception as E:
                    iface.messageBar().pushMessage(u"Fehler beim Speichern des Shapes im temporaeren Verzeichnis {}".format(E),
                                                   level=Qgis.Warning, duration=7)
    iface.messageBar().pushMessage(u"Gespeichert in {}".format(folderx),
                                                   level=Qgis.Info, duration=10)


def save_obj_compressed(name, obj):
    """Save a struct or list into a prickle binary file.

    Parameters
    ----------
    name : string
        path and filename for prickle file
        without file ending, functions add .pkl
    obj : struct, list,...
        object to save in prickle file
    """
    if len(name) > 6:
        if name[-5:] == '.blhw':
            name = name[:-5]
    try:
        with gzip.open(name + '.blhw', 'wb') as f:
            pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
            #print('saved')
    except:
        raise IOError('could not write prickle file {}'.format(name))


def load_obj_compressed(name):
    """Load a prickle binary file.

    Parameters
    ----------
    name : string
        path and filename for prickle file

    Returns
    -------
    temp : undefined
        whatever was saved in the compressed file (beware, could be malicious).
    """
    if len(name) > 6:
        if name[-5:] == '.blhw':
            name = name[:-5]
    try:
        with gzip.open(name + '.blhw', 'rb') as f:
            if sys.version_info[0] < 3:
                temp = pickle.load(f)
            else:
                temp = pickle.load(f, encoding='bytes')
        filename =  "".join([x if x.isalnum() else "_" for x in temp['applicationNumber']])
        folderx = os.path.join(SETUP.TEMPFOLDER, filename)
        if not temp['wellCatchment'] == '(nicht gesetzt)':
            load_layer('Bilanzgebiet', set_active=False, folderx=folderx, altName=temp['wellCatchment'])
        if not temp['balanceCatchment'] == '(nicht gesetzt)':
            load_layer('BrunnenEGZ', set_active=False, folderx=folderx, altName=temp['balanceCatchment'])

        return temp
    except Exception as e:
        raise e


def read_selectedUser(layer, selectedFeatures, userDict):
    """Read the user data from the wnv_land or wnv_lhw layers.

    Parameters
    ----------
    layer : layer object
        the layer with user data
    selectedFeatures : selectedFeatures object
        a selection from the layer
    userDict : dict
        the user dictionary

    Returns
    -------
    userDict : dict
        the updated user dictionary
    """
    cur_name = layer.name()

    if isinstance(selectedFeatures, list):
        selectedFeatures = selectedFeatures[0]

    id_id = layer.fields().lookupField(SETUP.DB_CONF[cur_name]['id'])
    userDict['ID'] = selectedFeatures.attributes()[id_id]
    userDict['TG'] = well_within_TG(selectedFeatures.geometry())

    for field in SETUP.DB_CONF[cur_name]['fields'].items():
        ### field[1] is column in data base / field[1] is key in userDict
        idx_tmp = layer.fields().lookupField(field[0])
        if idx_tmp == -1:
            userDict[field[1]] = '--'
            continue
        tmp = selectedFeatures.attributes()[idx_tmp]
        #print(idx_tmp, field, tmp, type(tmp))
        if field[1] in ('date_a', 'date_sn', 'date_b', 'befr_zeitr'):
            if not isinstance(tmp, unicode):
                tmp = tmp.toString('yyyy-MM-dd')
            try:
                tmp = datetime.datetime.strptime(tmp, '%Y-%m-%d')
            except:
                try:
                    tmp = datetime.datetime.strptime(tmp, '%Y-%m-%dT%H:%M')
                except:
                    tmp = datetime.datetime(1900, 1, 1)
        #print(tmp, type(tmp))
        userDict[field[1]] = tmp

    return userDict


def read_selectedUserQL(layer, selectedFeatures, userDict):
    """Read the QL value for a selected user (well point).

    Parameters
    ----------
    layer : layer object
        the layer with user data
    selectedFeatures : selectedFeatures object
        a selection from the layer
    userDict : dict
        the user dictionary

    Returns
    -------
    userDict : dict
        the updated user dictionary
    """
    if isinstance(selectedFeatures, list):
        selectedFeatures = selectedFeatures[0]
    idx_ql = layer.fields().lookupField( "QL_SPENDE" )
    idx_qleff = layer.fields().lookupField( "QL_EFF_SPE" )
    userDict['QL'] = selectedFeatures.attributes()[idx_ql]
    userDict['QL_EFF'] = selectedFeatures.attributes()[idx_qleff]
    return userDict


def add_attrDict(layer, inDict):
    """Add attributes to a layer.

    Parameters
    ----------
    layer : layer object
        the layer to add data to
    inDict : dict
        the key name is the attribute name of the attribute
    """
    feat = QgsFeature(layer.pendingFields())
    for item in inDict:
        feat.setAttribute(item, inDict[item])
    layer.dataProvider().addFeatures([feat])


def modify_attrDict(layer, inDict):
    """Modify attributes for all features in the layer.

    Parameters
    ----------
    layer : layer object
        the layer to modify
    inDict : dict
        the key name is the attribute name of the feature
    """
    layer.startEditing()
    for feature in layer.getFeatures():
        for item in inDict:
            feature[item] = inDict[item]
            layer.updateFeature(feature)
    layer.commitChanges()


def modify_attrDict_featList(layer, fidList, inDict, editHere=True):
    """Modify attributes of all selected features in the layer.

    Parameters
    ----------
    layer : layer object
        the layer to modify
    fidList : list
        a list of IDs for filtering
    inDict : dict
        the key name is the attribute name of the feature
    editHere : bool
        set Edititing mode. What happens if False???
    """
    request = QgsFeatureRequest().setFilterFids(fidList)
    #print(fidList, inDict)
    if editHere:
        layer.startEditing()
    for feature in layer.getFeatures(request):
        for item in inDict:
            feature[item] = inDict[item]
            layer.updateFeature(feature)
    if editHere:
        layer.commitChanges()


def check_layer_loaded(layer_name):
    """Check if layer with layer_name is loaded.

    Parameters
    ----------
    layer_name : str
        the name of the layer

    Returns
    -------
    tmp : bool
        True if layer is registered
    """
    boolean = False
    layers = QgsProject.instance().mapLayers()
    for name, layer in layers.items():
        if layer.name() == (layer_name):
            boolean = True
            break
    return boolean


def load_layer(layer_name, set_active=False, folderx=None, altName=None):
    """Load an shape layer_name.

    Parameters
    ----------
    layer_name : str
        the name of the layer
    set_active : bool, optional
        and set the loaded layer active if True
    folderx : str, optional
        provide an alternative path to the ESRI layer file
    altName : str, optional
        provide an alternative layer name when loaded

    Returns
    -------
    layer : layer object
        the loaded layer
    """
    if folderx is None:
        folderx = SETUP.BASEFOLDER
    if altName is None:
        altName = layer_name
    try:
        if not check_layer_loaded(layer_name):
            #print('trying to load {}'.format(os.path.join(folderx, layer_name + ".shp")))
            layer = QgsVectorLayer(os.path.join(folderx, layer_name + ".shp"), altName, "ogr")
            QgsProject.instance().addMapLayer(layer)
        else:
            layer = QgsProject.instance().mapLayersByName(layer_name)[0]
        if set_active:
            iface.setActiveLayer(layer)
        return layer
    except:
        iface.messageBar().pushMessage('Konnte Layer {} nicht laden.'.format(layer_name),
                                        level=Qgis.Warning, duration=7)


def getLayerHandle(layer_name,  set_active=False, folderx=None, altName=None):
    """Load an shape layer_name.
    
    Look if layer is loaded and return the layer object or load the layer otherwise.

    Parameters
    ----------
    layer_name : str
        the name of the layer
    set_active : bool, optional
        and set the loaded layer active if True
    folderx : str, optional
        provide an alternative path to the ESRI layer file
    altName : str, optional
        provide an alternative layer name when loaded

    Returns
    -------
    layer : layer object
        the loaded layer
    """
    if not check_layer_loaded(layer_name):
        return load_layer(layer_name, set_active=False, folderx=None, altName=None)
    else:
        return QgsProject.instance().mapLayersByName(layer_name)[0]


def regAandActi_layer(layer):
    """Register the layer and set it active."""
    QgsProject.instance().addMapLayer(layer)
    iface.setActiveLayer(layer)


def get_featuresReg(layer_name, get_id=None):
    """Get the features of a layer.
    
    If the layer is not registered, look in the
    SETUP.BASEFOLDER for a file with the layer_name.

    Parameters
    ----------
    layer_name : str
        the name of the layer
    get_id : int
        the ID of the feature

    Returns
    -------
    tmp : features object
        all features of the layer(get_id is None)
        or a specific feature (get_id is int)
    """
    loaded = check_layer_loaded(layer_name)
    if loaded:
        vl = QgsProject.instance().mapLayersByName(layer_name)[0]
        #print(vl)
    else:
        vl = QgsVectorLayer(os.path.join(SETUP.BASEFOLDER, layer_name + ".shp"), layer_name, "ogr")
    if get_id is None:
        return vl.getFeatures()
    else:
        for feat in vl.getFeatures():
            print(feat)
       # request = QgsFeatureRequest().setFilterFid(get_id)
       # feat = vl.getFeatures(request).next()
            return feat


def readListFromArcEGMO(fileName, colName, stripWhat=None):
    """Read an table from an ArcEGMO file.

    Parameters
    ----------
    filename : str
        path and filename
    colName : str
        the name of the column to read
    stripWhat : str, optional
        strip any specific chars from the input

    Returns
    -------
    incList : list
        list of the read values under the given column name
    """
    with open(fileName, 'r') as fid:
        fileH = csv.DictReader(fid, delimiter = '\t')
        incList = []
        if stripWhat is None:
            for line in fileH:
                incList.append(float(line[colName]))
        else:
            for line in fileH:
                if not line[colName] is None:
                    incList.append(float(line[colName].strip('-')))
        return incList


def sublayer(layer, incList, attributeName, name, showLayer=True):
    """Create an new layer with an subset of features from another layer.

    Parameters
    ----------
    layer : layer object
        layer with the superset of features
    incList : list
        list if TGIDs or other (that may be in attributeName)
    attributeName : str
        name of the field that is compared against incList
    name : str
        name of the new layer
    showLayer : bool, optional
        show the layer on the map if True

    Returns
    -------
    memlayer : layer object
        the new layer with the subset on features
    """
    feats = [feat for feat in layer.getFeatures()]
    name = layer.name() + '_new'
    ### check out geometry type of layer
    if layer.wkbType() == QgsWkbTypes.Polygon:
        mem_layer = QgsVectorLayer("Polygon?crs=epsg:" \
                                   + str(SETUP.CRS), name, "memory")
    elif layer.wkbType() == QgsWkbTypes.Point:
        mem_layer = QgsVectorLayer("Point?crs=epsg:" \
                                   + str(SETUP.CRS), name, "memory")
    elif layer.wkbType() == QgsWkbTypes.LineString:
        mem_layer = QgsVectorLayer("LineString?crs=epsg:" \
                                   + str(SETUP.CRS), name, "memory")

    mem_layer_data = mem_layer.dataProvider()
    #print('sl', layer.name())
    attr = layer.dataProvider().fields().toList()
    mem_layer_data.addAttributes(attr)
    mem_layer.updateFields()
    mem_layer_data.addFeatures(feats)

    feats = mem_layer.getFeatures()
    tgid = mem_layer.fields().lookupField(attributeName)
    with edit(mem_layer):
        for i, feat in enumerate(feats):
                if not float(feat.attributes()[tgid]) in incList:
                    #print(type(feat.attributes()[tgid] ))
                    mem_layer.deleteFeature(feat.id())
    if showLayer:
        mem_layer.updateExtents()
        QgsProject.instance().addMapLayer(mem_layer)

    return mem_layer


def getPathOfLayer(layer):
    """Return the path of the layer.

    Parameters
    ----------
    layer : layer object
        look for this layer

    Returns
    -------
    tmp : str
        the path
    """
    return layer.dataProvider().dataSourceUri().split('|')[0]


#def progdialog(progress, iteration):
#    dialog = QProgressDialog()
#    dialog.setWindowTitle("Fortschritt")
#    dialog.setLabelText("Iteration {}".format(iteration))
#    bar = QProgressBar(dialog)
#    bar.setTextVisible(True)
#    bar.setValue(progress)
#    bar.setRange(0,0)
#    dialog.setBar(bar)
#    dialog.setMinimumWidth(300)
#    dialog.show()
#    return dialog, bar

