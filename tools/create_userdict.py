# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 14:20:20 2020

@author: ruben.mueller
"""


import datetime

def createUserContainer(debug, setup_):
    if debug:
        JAHR_TM3 = 33
        BC = 'Brunneneinzugsgebiet1'
        TAG_M3 = 2.0
        BIL_JAHR_TM3 = 33
        x = 778521.468
        y =  5742180.765
    else:
        JAHR_TM3 = 0.0
        BC = '(nicht gesetzt)'
        TAG_M3 = 0.0
        BIL_JAHR_TM3 = 0.0
        x = 0.0
        y = 0.0
    
    userDict = {
    'mode': 0,
    'userName': u' ', #'undefiniert',
    'applicationNumber': u' ', #'undefiniert',
    'coordinateX': x,
    'coordinateY': y,
    'coordinateInitX': x,
    'coordinateInitY': y,
    'coordinateEndX': 0, #672900,
    'coordinateEndY': 0, #5831500,
    'staticLCalc': False,
    'total_addBalance': 0,
    'names_addBalance': [],
    'list_addBalance': [],
    'extractionType': u'Y', ### Y annual, F monthly percent, M monthly amount
    'extractionPercents': [0, 0, 0, 5, 10, 25, 25, 25, 10, 0, 0, 0],
    'extractionRates': [0 for i in range(12)],
    'JAHR_TM3': JAHR_TM3,
    'BIL_JAHR_TM3': BIL_JAHR_TM3,
    'days': setup_.EXTRDAYS,
    'DAY_M3': TAG_M3,
    'gwRechargeComponents': [1, 1, 1, 0], ## rg2 rg1 rdrain rh
    'catchmentWell': u'standard',
    'catchmentBalance' : u'standard',
    'currentStatus': 0, ### 0 în process, 1 rejected, 2 accepted,
    'currentStatusT': 'in Bearbeitung', ### 0 în process, 1 rejected, 2 accepted,
    'report': {
               'comments': '',
               'settings': [0, 0, 0]},
    'folderPermanent': 'default',
    'folderTemporary': 'default',
    'ID': 'default',
    'TG': 'default',
    'balanceCatchment': BC,
    'wellCatchment': '(nicht gesetzt)',
    'balanceCatchmentOrig': 'default',
    'wellPoint': '(nicht gesetzt)',
    'JAHR_TM3 total': 0,
    'GWR total': 0,  ### GWR from accounting,
    'QL': 0,
    'QL_eff': 0,
    'QL_selection': 0,
    'lastWellLayerNr': 1,
    'uIC': [], #[([], [], [], [], [], [], [], [])],
    'TM3_JAHR uIC' : 0,
    'layerName': None,
    'GWR flex': 0, ### GWR from flexible and fixed end point catchment computation
    'fromLayer': 'new',
    #########
    ## DB STUFF
    #########
    'lkr': '--',
    'userVorName': '',#'undefiniert',    
    'gemeinde': '--',
    'owk_st': '--',
    'gwk_st': '--',
    'tempPath': 'c:\\',
    'ezg_name': '',
    'bilanzgebiet': '',
    'b_qm_h': 0,
    'b_qm_d': 0,
     #'b_qm_a': [None, 0.5],
    'b_mit_tqm_a': 0,
    'b_max_tqm_a': 0,
    'g_qm_h': 0,
    'g_qm_d': 0,
     #'g_qm_a': [None, 0.5],
    'g_mit_tqm_a': 0,
    'g_max_tqm_a': 0,
    'bemerkungen': '',
    'bearb_lhw': '',
    'gw_stock':  '',
    'gw_schutz': '',
    'date_a': datetime.datetime(2000, 1, 1),
    'date_sn': datetime.datetime.now(),
    'date_b': datetime.datetime(2000, 1, 1),
    'nutzart': '',
    'nutzzweck': '',
    'gewaesser': '',
    'area': 1,
    'gemark': '',
    'flur': '',
    'flurst': '',
    'bescheid': '',
    'befr_zeitr': datetime.datetime(2000, 1, 1),
    'bereg_flaech': 0,
    'pegel': '',
    'einstellwert': 0,
    'results_GWN': {'GWR': 0, 'Area': 0, 'RG1': 0, 'RG2': 0,
                    'RH': 0, 'RD': 0, 'checks': 0},
    'bilance_text': 'Bilanz noch nicht berechnet',
    'path_statement' : 'c:\\',
    'account_text': '',
    'failed_intersect': []
    }
    return userDict