# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Provides the functionality to iteratively calculate the well catchment given
    spactially distributed ground waster recharge fields iteratively.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de

    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""


#############################################
### DEBUG SWITCH
#############################################
DEBUG = False
#############################################

import math
import os
import processing
from qgis.core import (QgsProject, QgsDistanceArea, QgsCoordinateReferenceSystem,
    QgsGeometry, QgsPointXY, QgsFeature, QgsField,
    QgsRasterLayer, Qgis, QgsFeatureRequest, QgsVectorLayer, NULL)
from PyQt5.QtCore import QSettings
from PyQt5.QtCore import QVariant
from qgis.utils import iface
#from qgis.analysis import QgsZonalStatistics

from .qgis_toolbox import check_layer_loaded
from .. setup import setup__ as SETUP

from PyQt5.QtCore import QFileInfo


POLYGONLAYER = "Polygon?crs=EPSG:{}".format(SETUP.CRS)
POINTLAYER = "Point?crs=EPSG:{}".format(SETUP.CRS)

def string_to_raster(raster):
    # Check if string is provided

    fileInfo = QFileInfo(raster)
    path = fileInfo.filePath()
    baseName = fileInfo.baseName()
    if check_layer_loaded(baseName):
        layer = QgsProject.instance().mapLayersByName(baseName)[0]
    else:
        layer = QgsRasterLayer(path, baseName)
        #QgsProject.instance().addMapLayer(layer)

    if layer.isValid() is False:
        print("Unable to read basename and file path - Your string is probably invalid")
    return layer


def get_position_of_catchment_end(active=False):
    '''get the position of the endpoint of the catchment

    Returns
    -------
    position_of_catchmant_end : list
        [x-axis coordinates, y-axis coordinates] of catchment end point'''
    try:
        if active:
            end_point = active
            load_layer = active.name()
        else:
            load_layer = SETUP.ENDPOINT
            if not check_layer_loaded(load_layer):
                iface.messageBar().pushMessage(u"Kein Layer \"{}\" geladen.".format(load_layer),
                            level=Qgis.Info, duration=3)
                return None

            end_point = QgsProject.instance().mapLayersByName(load_layer)[0]
        end_point_Features = end_point.getFeatures()

        for end_point_Feature in end_point_Features:
            end_point_Geometrie = end_point_Feature.geometry()
            position_of_catchmant_end = end_point_Geometrie.asPoint()
    except:
        iface.messageBar().pushMessage(u"Kein Endpuntk von Layer \"{}\" lesbar.".format(load_layer),
                            level=Qgis.Info, duration=3)
    return position_of_catchmant_end



def get_shape_parameter():
    '''returns a dictionary with the parameters a, b and F_threshold'''
    A = 20
    B = 0.18
    F_threshold = 1 + A*(0.0005)**B
    shape_parameters = {'a': A, 'b': B, 'F_treshold': F_threshold}
    return shape_parameters



def get_Iteration_Parameter():
    '''returns a dictionary with the default parameter set of the iterative search'''
    ipDict = {'iterMax': SETUP.ITERMAX, ### since it's so fast now...
                'Tol': SETUP.EPSITER,
                'factorP': 0.5,
                'relaxUnder': 5,
                'relaxUnderIter1': SETUP.RELAXB1, # 30, ### we should never get to this stage
                'relaxUnderIter2': SETUP.RELAXB2, # 50, ### very unlikely, if we reach this, then it is borked anyway...
                'relaxF1': 0.5,
                'relaxF2': 0.35}
    return ipDict


def get_layer(layername):
    '''return the layer object belonging to the layername

    Parameters
    ----------
    layername : str
        the name of the layer'''
    mapCanvas = iface.mapCanvas()
    layers = mapCanvas.layers()
    layer_layername = None
    for layer in layers:
        if layer.name() == layername:
           layer_layername = layer
    if layer_layername is None:
        iface.messageBar().pushMessage(u"Layer {} nicht verfügbar. Deaktiviert?".format(layername),
                                       level=Qgis.Warning, duration=3)
    else:
        return layer_layername

def find_direction(cosvalue, sinvalue):
    '''calculate the direction angle from cos and sin

    Parameters
    ----------
    cosvalue : float
        the cosine
    sinvalue : float
        the sine

    Returns
    -------
    direction : float
        the direction'''
    tanalpha = sinvalue/cosvalue
    if tanalpha > 0:
        if sinvalue > 0:
            direction = math.degrees(math.atan(tanalpha))
        else:
            direction = math.degrees(math.atan(tanalpha))+180

    if tanalpha < 0:
        if sinvalue > 0:
            direction = math.degrees(math.atan(tanalpha))+180
        else:
                direction = math.degrees(math.atan(tanalpha))

    return direction


def get_bufferGeometry(wellDict, iterDict):
    '''create a buffer around the well point and return the geometry object

    Parameters
    ----------
    wellDict : dict
        contains the informations about the well
    iterDict : dict
        contains the information about parameters at the current iteration

    Returns
    -------
    tmp : buffer geometry object
        the buffer around the well'''

    bufferGeometry = QgsGeometry.fromPointXY(QgsPointXY(wellDict['coordinateX'],
        wellDict['coordinateY']))
    return bufferGeometry.buffer(iterDict['R'], 5)


def is_within_area(bufferGeometry):
    '''check if the bufferGeometry intersects the catchment

    Parameters
    ----------
    bufferGeometry : buffer geometry object
        the buffer around the well

    Returns
    tmp : bool
        true if bufferGeometry is within the area, false otherwise'''

    untersuchungsgebiete = get_layer("untersuchungsgebiet")
    gebiet_untersuchung_Features = untersuchungsgebiete.getFeatures()
    for polyFeat_innner in gebiet_untersuchung_Features:
        polyGeom_inner = polyFeat_innner.geometry()
        # Geometrie von Buffer
        if bufferGeometry.within(polyGeom_inner):
            return True
        else:
            return False

### NEW: get_meanvalue --> use processing
def get_gradient_and_direction(bufferGeometry, iterDict):
    '''calculate the mean direction and the gradient for a geometry

    Parameters
    ----------
    bufferGeometry : buffer geometry object
        the buffer around the well
    iterDict : dict
        contains the information about parameters at the current iteration

    Returns
    -------
    iterDict : dict
        the updated iteration dictionary'''
    QSettings()
    well_buffer = QgsVectorLayer(POLYGONLAYER, "pointbuffer", "memory")
    feature = well_buffer.dataProvider()
    poly = QgsFeature()

    poly.setGeometry(bufferGeometry)
    feature.addFeatures([poly])
    well_buffer.updateExtents()

    tmplayer = string_to_raster(os.path.join(SETUP.BASEFOLDER, 'gefaelle.tif'))
    gefaelle = get_meanvalue(well_buffer, tmplayer)

    tmplayer = string_to_raster(os.path.join(SETUP.BASEFOLDER, 'cos.tif'))
    cos = get_meanvalue(well_buffer, tmplayer)

    tmplayer = string_to_raster(os.path.join(SETUP.BASEFOLDER, 'sin.tif'))
    sin = get_meanvalue(well_buffer, tmplayer)

    direction = find_direction(cos, sin)

    iterDict['slope'] = gefaelle/100
    iterDict['direction'] = direction

    return iterDict


def get_meanvalue(wcatch, rlayer):
    '''calculate the areal mean with ZonalStatistics

    Parameters
    ----------
    wcatch : vector layer
        the well catchment
    rlayer : raster layer
        the raster layer

    Returns
    meanvalue : float
        the calculated mean
    '''
    pd = {"INPUT": wcatch, "INPUT_RASTER": rlayer, 
    "STATISTICS": [2], "RASTER_BAND": 1,
    "OUTPUT": "TEMPORARY_OUTPUT"}
    out = processing.run("qgis:zonalstatisticsfb", pd)["OUTPUT"]
    meanvalue = next(out.getFeatures())["_mean"]
    #print(meanvalue)
    return meanvalue
##################################################################

def get_parameter_of_well_catchment(wellDict, iterDict):
    '''calulate various parameters necessary for the catchment geometry

    Parameters
    ----------
    wellDict : dict
        contains the informations about the well
    iterDict : dict
        contains the information about parameters at the current iteration

    Returns
    -------
    iterDict : dict
        the updated iteration dictionary'''

    shpp = get_shape_parameter()
    iterDict['F'] = 1 + shpp['a'] *(iterDict['slope'])**shpp['b']
    if not iterDict['staticL']:
        iterDict['L'] = (1000000 * wellDict['Q'] * iterDict['F'] / iterDict['GWR']) ** 0.5
        # die Laenge der Ende der Stromlinie
        iterDict['endX'] = (math.sin((iterDict['direction'] + 180) * math.pi / 180)) \
            *iterDict['L'] + wellDict['coordinateX']
        # Position der Ende der Bahnlinien (x-Koordinate)
        iterDict['endY'] = (math.cos((iterDict['direction'] + 180) * math.pi / 180)) \
            *iterDict['L'] + wellDict['coordinateY']
    # Position der Ende der Bahnlinien (y-Koordinate)
    iterDict['Xk'] = (iterDict['L'] ** 2 \
        + (2 * 1000000 * wellDict['Q'] / math.pi/iterDict['GWR'])) ** 0.5 - iterDict['L']
    iterDict['Bmax'] = 2 * ((((iterDict['L'] + iterDict['Xk']) ** 2) \
        + (2*(math.pi + 2) * 1000000 * wellDict['Q'] / iterDict['GWR'])) ** 0.5 \
        - iterDict['L'] - iterDict['Xk']) / (math.pi + 2)
    iterDict['Xbmax'] = ((1000000 * wellDict['Q'] / math.pi / iterDict['GWR']) \
        - ((iterDict['Bmax'] ** 2) / 4)) ** 0.5

    return iterDict


def get_polygon_of_well_catchment(iterDict, wellDict):
    '''calculate the well catchment geometry

    Parameters
    ----------
    wellDict : dict
        contains the informations about the well
    iterDict : dict
        contains the information about parameters at the current iteration

    Returns
    -------
    catchmentGeometry : geometry object
        the well catchment geometry'''

    shapeParameter = get_shape_parameter()
    QSettings()

    X_k = iterDict['Xk']
    L = iterDict['L']
    B_MAX = iterDict['Bmax']
    ende_x = iterDict['endX']
    ende_y = iterDict['endY']
    X_bmax = iterDict['Xbmax']

    xB = [-X_k, -0.8*X_k, 0, X_bmax/3, X_bmax, L/2, 0.75*L, 0.9*L, L, \
          0.9*L, 0.75*L, L/2, X_bmax, X_bmax/3, 0, -0.8*X_k ]
    yB = [0, 0.8*X_k, 1.5*X_k, 0.3*(B_MAX-(3*X_k))+(1.5*X_k), 0.5*B_MAX, 1.9*X_k, \
          1.1*X_k, 0.44*X_k, 0, -0.44*X_k, -1.1*X_k, -1.9*X_k, -0.5*B_MAX, \
          -0.3*(B_MAX-(3*X_k))-(1.5*X_k), -1.5*X_k, -0.8*X_k]

    if iterDict['F'] > shapeParameter['F_treshold']:
        # gefaelle = gradient_threshold_Interpolation
        # wenn Gefaell genug gross ist
        # Reihe von temporaeren Konstruktionspunkte des tropfenfoermigen Einzugsgebiets
        x = xB
        y = yB
    else:
        # Reihe von temporaeren Konstruktionspunkte des kreisfoermigen Einzugsgebiets
        winkel = list(range(16))
        R = (1000000 * wellDict['Q'] / iterDict['GWR'] / math.pi) ** 0.5
        x_rund= list(range(16))
        y_rund= list(range(16))
        x = list(range(16))
        y = list(range(16))

        for d in range(16):
            # Reihe von temporaeren Konstruktionspunkte des kreisfoermigen Einzugsgebiets
            winkel[d] = -0.5*math.pi+((d)*math.pi/8)
            x_rund[d] = R*math.sin(winkel[d])
            y_rund[d] = R*math.cos(winkel[d])

               # Kombination durch Interpolation
            x[d] = ((shapeParameter['F_treshold']-iterDict['F'])*x_rund[d] \
                   + ((iterDict['F']-1)*xB[d]))\
                   / (shapeParameter['F_treshold'] - 1)
            y[d] = ((shapeParameter['F_treshold']-iterDict['F'])*y_rund[d] \
                   + ((iterDict['F']-1)*yB[d]))\
                   / (shapeParameter['F_treshold'] - 1)

    # mit Rotationsmatrix zur originalen Kartesische Koordinaten drehen
    # Winkel zischen urspruenglichem Koordinatensystem und neuem Koordinatensystem (gegen Uhrzeigersinn)
    # theta: Winkel
    costheta = (wellDict['coordinateX']-ende_x) \
        / (((wellDict['coordinateX']-ende_x)**2 \
        + (wellDict['coordinateY']-ende_y)**2)**0.5)
    sintheta = (wellDict['coordinateY']-ende_y) \
        / (((wellDict['coordinateX']-ende_x)**2 \
        + (wellDict['coordinateY']-ende_y)**2)**0.5)

    # Erzeugung einer Tabelle fuer die Konstruktionspunkte zur Abgrenzung eines Brunneneinzugsgebietes
    points = list(range(16))

    for d3 in range(16):
        points[d3] = QgsPointXY(-1*x[d3]*costheta+y[d3]*sintheta + wellDict['coordinateX'],
                                -1*y[d3]*costheta-x[d3]*sintheta + wellDict['coordinateY'])
    catchmentGeometry = QgsGeometry.fromPolygonXY([points])
    return catchmentGeometry


def get_well_catchment(catchmentGeometry):
    '''create a layer from catchment geometry

    Parameters
    ----------
    catchmentGeometry : geometry object
        the well catchment geometry

    Returns
    -------
    new_well_catchment : layer object
        the polygon layer for the well catchment'''
    QSettings()
    new_well_catchment = QgsVectorLayer(POLYGONLAYER, "jiushiniubi" , "memory")
    feat = new_well_catchment.dataProvider()
    poly = QgsFeature()
    poly.setGeometry(catchmentGeometry)
    feat.addFeatures([poly])
    new_well_catchment.updateExtents()
    return new_well_catchment



def draw_catchment(well_catchment):
    '''register and show the layer in gis

    Parameters
    ----------
    well_catchment : layer object
        the well layer'''

    QSettings()
    QgsProject.instance().addMapLayers([well_catchment])
    well_catchment.updateExtents()
    QgsProject.instance().addMapLayer(well_catchment)
    iface.mapCanvas().refresh()

#################################################
def checkv(v):
    return True if isinstance(v, (float, int)) else False

def get_recharge_from_element(catchment_geometry, check_wert, efl_layer=None, create_layer=False):
    '''cycles through all features of the EFL-layer and computes the total GWR
    of of all EFL that are within the catchment

    Parameters
    ----------
    catchment_geometry : geometry object
        geometry of the well catchment
    check_wert : list
        multipliers (0 or 1) for the flow components.
        The order is cruical (RG2, RG1, Rdrain, RH)!

    Returns
    -------
    gwr_catchment : float
        the mean ground water recharge of the well catchemnt'''

    QSettings()
    if efl_layer is None:
        efl_layer = get_layer(SETUP.EFLLAYER)

    id_rg2_efl = efl_layer.fields().lookupField( "RG2" )
    id_rg1_efl = efl_layer.fields().lookupField( "RG1" )
    id_drain_efl = efl_layer.fields().lookupField( "DRAIN" )
    id_rh_efl = efl_layer.fields().lookupField( "RH" )
    GWN = 0
    RG1 = 0
    RG2 = 0
    RD = 0
    RH = 0
    weight_t = 0
    failed = 0
    tempRHx = 0
    tempRDx = 0
    tempRG1x = 0
    tempRG2x = 0
    list_failed = []

    cands = efl_layer.getFeatures(QgsFeatureRequest().setFilterRect(catchment_geometry.boundingBox()))
    areac = catchment_geometry.area()

    if create_layer:
        mem_layer = QgsVectorLayer(POLYGONLAYER, 
            "Fehlerhafte_Flaechen", "memory")
        mem_layer_data = mem_layer.dataProvider()

    for i, area_feature in enumerate(cands):
        if area_feature.geometry().intersects(catchment_geometry):
            ueber = area_feature.geometry().intersection(catchment_geometry)
            weight = ueber.area()
            efl_Attributte = area_feature.attributes()
            # use area of intersection as weight
            if ueber is None or weight == -1:
                failed += 1
                list_failed.append(area_feature.id())
                tmp = efl_Attributte[id_rh_efl] 
                tempRHx += tmp if check_wert[3] else 0
                tmp = efl_Attributte[id_drain_efl]
                tempRDx += tmp if check_wert[2] else 0
                tmp = efl_Attributte[id_rg1_efl]
                tempRG1x += tmp if check_wert[1] else 0
                tmp = efl_Attributte[id_rg2_efl] 
                tempRG2x += tmp if check_wert[0] else 0
                if create_layer:
                    mem_layer_data.addFeatures([area_feature])
                    mem_layer.commitChanges()
            else:
                weight = 0 if weight == -1 else weight
                weight_t += weight
                
                # total of weights
                tmp = efl_Attributte[id_rh_efl]
                tempRH = tmp * weight if check_wert[3] and checkv(tmp) else 0
                tmp = efl_Attributte[id_drain_efl]
                tempRD = tmp * weight if check_wert[2] and checkv(tmp) else 0
                tmp = efl_Attributte[id_rg1_efl]
                tempRG1 = tmp * weight if check_wert[1] and checkv(tmp) else 0
                tmp = efl_Attributte[id_rg2_efl]
                tempRG2 = tmp * weight if check_wert[0] and checkv(tmp) else 0
 
                tempGWN = tempRH + tempRD + tempRG1 + tempRG2
                GWN += tempGWN
                RG1 += tempRG1
                RG2 += tempRG2
                RH += tempRH
                RD += tempRD

    if failed:
        adiff = areac - weight_t 
        mean_area = adiff / failed
        tempRHx *= mean_area
        tempRDx *= mean_area
        tempRG1x *= mean_area
        tempRG2x *= mean_area
        tempGWNx = tempRHx + tempRDx + tempRG1x + tempRG2x
        GWN += tempGWNx
        RG1 += tempRG1x
        RG2 += tempRG2x
        RH += tempRHx
        RD += tempRDx
        weight_t += adiff    
        if create_layer:
            mem_layer.updateExtents()
            QgsProject.instance().addMapLayer(mem_layer)
        
    print("failed-->", failed)
    if create_layer and failed:
        fmtstr = u"""Konnte {0} EFL oder TG nicht verschneiden (topologisch inkorrekte Geometry?).
Über {0} EFL/TG mit fehlerhaften Flächengrößen wurde gemittelt!"""
        iface.messageBar().pushMessage(fmtstr.format([failed]),
            level=Qgis.Warning, duration=7)
                
    return {'GWR': GWN / 1e6, 'Area': weight_t,
            'RG1': RG1 / 1e6, 'RG2': RG2 / 1e6,
            'RH': RH / 1e6, 'RD': RD / 1e6,
            'GWR_m2': GWN / weight_t,
            'checks': check_wert, 
            'failed_intersect': list_failed}


def getFactorRelaxation(ipDict, iteration):
    '''return the ralaxation factor

    Parameters
    ----------
    ipDict : dict
        the iteration specific parameters
    iteration : int
        the iteration number

    Returns
    -------
    tmp : float
        the relaxation factor'''

    if iteration > ipDict['relaxUnderIter1'] - 1:
        return ipDict['relaxF1']
    elif iteration > ipDict['relaxUnderIter2'] - 1:
            return ipDict['relaxF2']
    else:
        return 1


def Iteration (wellDict, check_wert, endpoint=[], staticL=False):
    '''iterative search for a well catchment for a given extraction rate and
    spacial distributed ground water recharge areas

    Parameters
    ----------
    wellDict : dict
        contains the data of the well
    check_wert : list
        multipliers (0 or 1) for the flow components.
        The order is cruical (RG2, RG1, Rdrain, RH)!
    endpoint : list, optional
        defines the endpoint for the fixed endpoint iteration.
        the list holds the X and Y value.
        the algorithm must not be empty if staticL is False
    staticL : bool
        if True, use a fixed endpoint for the well catchment

    Returns
    -------
    iterDict : dict
        the dictionary with the results of the last iteration'''

    iterDict = {'diff_GWR': 1, 'slope': 0, 'direction': 0, 
                'Xk': 0, 'L': 0, 'F': 0,
                'Bmax': 0, 'endX': 0, 'endY': 0, 
                'Xbmax': 0, 'GWR_o': wellDict['firstGWR'],
                'GWR': wellDict['firstGWR'], 'R_o': wellDict['firstRadius'],
                'R': wellDict['firstRadius'], 'staticL': False}
    ipDict = get_Iteration_Parameter()
    # Anzahl der Iteration
    list_iter = []
    if staticL:
        iterDict['endX'] = endpoint['coordinateEndX']
        iterDict['endY'] = endpoint['coordinateEndY']
        iterDict['L'] = ((wellDict['coordinateX'] - iterDict['endX']) ** 2 \
            + (wellDict['coordinateY'] - iterDict['endY']) ** 2)**0.5
        iterDict['staticL'] = True


    for iteration in range(0, ipDict['iterMax']):
        if DEBUG: print('Iteration', iteration)
        if iterDict['diff_GWR'] > ipDict['Tol']:
            #print('iteration', iteration)
            factorRelaxation = getFactorRelaxation(ipDict, iteration)

            bufferGeometry = get_bufferGeometry(wellDict, iterDict)

            if not is_within_area(bufferGeometry):
                if DEBUG: print('NOT IN AREA', ipDict['Tol'] - 1)

            if not iterDict['staticL'] or iteration == 0:
                iterDict = get_gradient_and_direction(bufferGeometry, iterDict)

            iterDict = get_parameter_of_well_catchment(wellDict, iterDict)
            catchmentGeometry = get_polygon_of_well_catchment(iterDict, wellDict)

            iterDict['GWR'] = get_recharge_from_element(catchmentGeometry, check_wert)['GWR_m2']

            if iterDict['GWR'] <= 0:
                iterDict['GWR'] = iterDict['GWR_o'] * ipDict['factorP']
                vergleichfaktor = -1
                # kennenwert fuer Beurteilung:
                # Wenn GWN negativ ist, vergleichfaktor = -1
                iterDict['diff_GWR'] = 5
                # eine grosse Differenz
            else:
                vergleichfaktor = 1
                iterDict['GWR'] = factorRelaxation * \
                    (iterDict['GWR'] - iterDict['GWR_o']) + iterDict['GWR_o']
                iterDict['diff_GWR'] = (abs(iterDict['GWR'] - iterDict['GWR_o']) \
                    / iterDict['GWR']) / factorRelaxation

            if DEBUG: print("diff_GWN {} / recharge_value {} / iterDict['GWR'] {} / Q {} / Area {}".format(iterDict['diff_GWR'],
                 iterDict['GWR_o'], iterDict['GWR'],
                 wellDict['Q'], catchmentGeometry.area()))

            vergleich= list(range(3))
            vergleich[0] = iterDict['GWR_o']
            vergleich[1] = vergleichfaktor
            vergleich[2] = iterDict['diff_GWR']
            #print(vergleich)
            list_iter.append(vergleich)
                # Dokument der Proyesse der Iteration

            iterDict['R'] = (1000000 * wellDict['Q'] / iterDict['GWR'] /math.pi)**0.5

            iterDict['GWR_o'] = iterDict['GWR']
            iterDict['R_o'] = iterDict['R']
        else:
            break

    if iterDict['diff_GWR'] > ipDict['Tol'] :
        print('PROBLEM diffGWR > TOL', iterDict['diff_GWR'], ipDict['Tol'])
        #  wenn Iterationszahl > Max. Iterationszahl und Diff > Toleranz
        iterDict['diff_GWR'] = 100
            # Anfangszahl
        for diff in list_iter:
            # Vergleich von ein Schritt zu ein Schritt
            if diff[1] == 1:
                # nur fuer positive GWN
                if diff[2] < iterDict['diff_GWR']:
                    # Suchung der kleinsten Differenz
                    iterDict['diff_GWR'] = diff[2]
                        # Suchung der kleinsten Differenz
                    iterDict['GWR'] =diff[0]
                    iterDict['R'] = (1000000 * wellDict['Q'] / iterDict['GWR'] /math.pi)**0.5

    return iterDict


def make_catchmentLayer(iterDict, wellDict, userDict, endpoint=[0, 0], crs=False):
    ''' return a layer with the already calculated well catchment

    Parameters
    ----------
    wellDict : dict
        contains the informations about the well
    iterDict : dict
        contains the information about parameters at the current iteration
    wellNr : int
        the counter for the created well layers
    Returns
    -------
    new_well_catchment : layer object
        the well catchment layer'''

    new_well_catchment = QgsVectorLayer(POLYGONLAYER,
        "Brunneneinzugsgebiet" + str(userDict['lastWellLayerNr']), "memory")
    if crs:
        new_well_catchment.setCrs(QgsCoordinateReferenceSystem(crs))
    feat = new_well_catchment.dataProvider()
    poly = QgsFeature()
    tmp = [QgsField("ID", QVariant.String),
           QgsField("RECHTSWERT",  QVariant.Double),
           QgsField("HOCHWERT", QVariant.Double),
           QgsField("GWN (mm)", QVariant.Double),
           QgsField("Flaeche", QVariant.Double),
           QgsField("Entnahmetage", QVariant.Double),
           QgsField("m3/Tag", QVariant.Double),
           QgsField("JAHR_TM3", QVariant.Double)]
    if endpoint:
        tmp +=[QgsField("Endpunkt_X", QVariant.Double),\
               QgsField("Endpunkt_Y", QVariant.Double)]
    feat.addAttributes(tmp)
    new_well_catchment.updateFields()

    catchmentGeometry = get_polygon_of_well_catchment(iterDict, wellDict)
    d = QgsDistanceArea()
    area = d.measurePolygon(catchmentGeometry.asPolygon()[0])
    poly.setAttributes([wellDict['ID'], wellDict['coordinateX'], wellDict['coordinateY'],
                        iterDict['GWR'], area/1000000, wellDict['extr_days'],
                        wellDict['m3_tag'], userDict['BIL_JAHR_TM3'],
                        endpoint[0], endpoint[1]])

    ### new geometry
    poly.setGeometry(catchmentGeometry)
    feat.addFeatures([poly])

    new_well_catchment.updateExtents()
    return new_well_catchment


def gwrSelection(attrs, idxDict, gwrComponents):

    '''look for not defined values in the attribute table
    and set them zero. Further, select only needed components to compute the
    recharge

    Parameters
    ----------
    attrs : attributes
        attributes of the layer
    idxDict : dict
        holds the indizes for the components
    gwrComponents : list
        contains zeros or ones to sum up only needed components
        odering is important (RG2, RG1, Rdrain, Rh)!
    Returns
    -------
    gwr : float
        the ground water recharge rate'''

    if attrs[idxDict['DRAIN']] == NULL:
        drain = 0
    else:
        drain = idxDict['DRAIN']
    if attrs[idxDict['RG2']] == NULL:
        rg2 = 0
    else:
        rg2 = attrs[idxDict['RG2']]
    if attrs[idxDict['RG1']] == NULL:
        rg1 = 0
    else:
        rg1 = attrs[idxDict['RG1']]
    if attrs[idxDict['RH']] == NULL:
        rh = 0
    else:
        rh = attrs[idxDict['RH']]
    gwr = rg1*gwrComponents[1] + rg2*gwrComponents[0] \
          + drain*gwrComponents[2] + rh*gwrComponents[3]
    return gwr



def first_estimate(areaSAFeatures, wellFeatures, tgFeatures,
                   idxDict, newWellDict, gwrComponents, other):
    '''first guess of well catchment based on TG layer

    Parameters
    ----------
    areaSAFeatures : features object
        federal state area
    wellFeatures : features object
        layer for the new well
    tgFeatures : features object
        sub catchments
    newWellDict : dict
        configuration of the new well
    idxDict : dict
        indices of attributes
    gwrComponents : list
        selected fluxes for groundwater recharge
    other : float
        additional balance components
    Returns
    -------
    newWellDict : dict
        the updated dictionary'''

    iface.messageBar().pushMessage(u"Erstschätzung für Brunneneinzugsgebiet...",
                    level=Qgis.Info, duration=3)

    ### should be one feature
    for wc, sWellFeature in enumerate(wellFeatures):
        wellGeometry = sWellFeature.geometry()
        ### should be one feature
        for sc, areaSAFeature in enumerate(areaSAFeatures):
            wellAttribute = sWellFeature.attributes()
            Q = float(wellAttribute[idxDict['JAHR_TM3']]) + other
            R = 0
            GWN = 0

            if wellGeometry.intersects(areaSAFeature.geometry()):
                for tgFeat in tgFeatures:
                    attrs = tgFeat.attributes()

                    if wellGeometry.intersects(tgFeat.geometry()):
                        GWN = gwrSelection(attrs, idxDict, gwrComponents)
                        GWN = float(GWN)

                        if GWN > 0:
                            break
            if GWN <= 0:
                GWN = 10

        R = (Q/GWN/3.1415926)**0.5
        try:
            newWellDict.update({'feasible': True,
                            'firstRadius': R,
                            'firstGWR': GWN,
                            'Q': Q,
                            'TGID': attrs[idxDict['TGID']]
                            })
        except UnboundLocalError:
            iface.messageBar().pushMessage(u"Fehler! Liegt Brunnen innerhalb des Gebiets?",
                        level=Qgis.Warning, duration=3)
            return

        iface.messageBar().pushMessage(u"... beendet",
                        level=Qgis.Info, duration=3)

        return newWellDict

    return newWellDict.update({'feasible': False})


def get_idxDictTgEfl(layer, idxDict={}, what='TGID'):
    '''prepare/update a dictionary for the new well with
    certain indices from the sub catchment layer

    Parameters
    ----------
    layer : layer object
        the well catchment layer
    idxDict : dict
        this dictionary is updated
    what : get index also for this field, also used as key in idxDict'''

    idxDict.update({'RG1': layer.fields().lookupField("RG1"),
                    'RG2': layer.fields().lookupField("RG2"),
                    'SICK': layer.fields().lookupField("SICK"),
                    'DRAIN': layer.fields().lookupField("DRAIN"),
                    'RH': layer.fields().lookupField("RH"),
                    what: layer.fields().lookupField(what)})
    return idxDict


def get_idxDictWell(layer, idxDict={}):

    '''prepare/update a dictionary for the new well with
    certain indices from the well layer

    Parameters
    ----------
    idxDict : dict
        the indizes of parameters in attribute table to update

    Returns
    -------
    idxDict : dict
        the updated dictionary'''

    idxDict.update({'coordinateY': layer.fields().lookupField('coordinateY'),
                    'coordinateX': layer.fields().lookupField('coordinateX'),
                    'JAHR_TM3': layer.fields().lookupField("JAHR_TM3"),
                    'wellTG': layer.fields().lookupField("TG"),
                    'wellID': layer.fields().lookupField("ID")})
    return idxDict



def get_QL(layer, idxDict={}):
    '''prepare/update a dictionary for the new well with
    certain indices from the sub catchment layer

    Parameters
    ----------
    layer : layer object
        the well catchment layer
    idxDict : dict
        this dictionary is updated
    what : get index also for this field, also used as key in idxDict'''

    idxDict.update({'QL_org': layer.fields().lookupField("QL_SPENDE"),
                    'QL_eff': layer.fields().lookupField("QL_EFF_SPE")})
    return idxDict


def create_wellPointLayer( newWellDict, crsID=None, name='neuer_Brunnen', geo3='JAHR_TM3'):
    '''create a layer for the new well'''
    newWell = QgsVectorLayer(POINTLAYER, name, "memory")

    if not crsID is None:
        newWell.setCrs(QgsCoordinateReferenceSystem(crsID))

    feat = newWell.dataProvider()
    # Erzeugung der Attribute-Tabelle
    feat.addAttributes([QgsField("ID", QVariant.String),
                        QgsField('coordinateX',  QVariant.Int),
                        QgsField('coordinateY', QVariant.Int),
                        QgsField(geo3, QVariant.Double)]
                      )
    newWell.updateFields()
    newPoint = QgsPointXY(newWellDict['coordinateX'], newWellDict['coordinateY'])

    poly = QgsFeature()
    poly.setAttributes([newWellDict['ID'],
                        newWellDict['coordinateX'],
                        newWellDict['coordinateY'],
                        newWellDict[geo3]])
    poly.setGeometry(QgsGeometry.fromPointXY(newPoint))
    feat.addFeatures([poly])
    feat.updateExtents()
    return newWell








