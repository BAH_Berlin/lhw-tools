# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 11:40:23 2018

@author: Ruben.Mueller
"""

import os
import sys
import pickle
import gzip
import future

def load_obj_compressed(name):
    '''loads a prickle binary file

    Parameters
    ----------
    name : string
        path and filename for prickle file

    Returns
    -------
    temp : undefined
        whatever was saved in the compressed file (beware, could be malicious).'''

    if len(name) > 6:
        if name[-5:] == '.blhw':
            name = name[:-5]
    try:
        with gzip.open(name + '.blhw', 'rb') as f:
            if sys.version_info[0] < 3:
                temp = pickle.load(f)
            else:
                temp = pickle.load(f, encoding='bytes')
        return temp
    except Exception as e:
        raise e

x = 'S:\\sa_program\\74_ba_2018_70639\\'#'C:\\Users\\Ruben.Mueller\\.qgis2\\temp\\123'

userDict = load_obj_compressed(os.path.join(x, 'project'))