# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 13:03:33 2020

@author: ruben.mueller
"""
import os
from qgis.core import (Qgis, QgsFeatureRequest)
from qgis.PyQt.QtWidgets import QDialog

from .setup import setup__ as SETUP
from .tools import qgis_toolbox
from qgis.core.additions.edit import edit
from .pyqtDialogs.dialog_AD_delete import Ui_Dialog_admin_delete

#print(dir(database_toolbox))

def setCB(combobox, fieldnames, default=False):
    """Populate the text of a comboBox.
    
    Parameters
    ----------
    combobox : QComboBox
        the combobox to fill with texts
    fieldnames : list
        a list with str --> text to set in the combobox
    default : str
        sets the combobox to this selection, if exists
    """
    combobox.clear()

    for fld in fieldnames:
       combobox.addItem(str(fld))
    index = 0
    if default:
        index = combobox.findText(default)
        if index >= 0:
             combobox.setCurrentIndex(index)
    return index


def filter_attr(layer, field, value):
    """Return the first feature equaling a value in a field.
    
    Parameters
    ----------
    layer : layer
        the layer to check
    field : str
        the field name
    value : str, int, float
        the value to check against

    Returns
    -------
    feat : feature
        the hopefully right feature or False if none is found
    """
    expr = '"{}"={}'.format(field, value)
    request = QgsFeatureRequest().setFilterExpression(expr)
    try:
        feat = next(layer.getFeatures(request))
    except:
        feat = False
    return feat


class Start_admin_delete(QDialog, Ui_Dialog_admin_delete):
    '''class for the main dialog of the stand alone water balance dialog'''

    def __init__(self, parent, db_rw):
        super().__init__(parent.mainWindow())
        self.setupUi(self)
        self.db_rw = db_rw
        self.iface = parent
        self.pushButton_delete.clicked.connect(self.delete_entry)   
    
    def showEvent(self, event):
        """update the combobox when dialog is opening"""
        self.layer_name_ezg = SETUP.DB_EZG["table"]      
        self.layer_ezg = qgis_toolbox.getLayerHandle(self.layer_name_ezg)
        if not self.layer_ezg:
            self.iface.messageBar().pushMessage(u"Bitte erst Layer bereitstellen. Fenster schließen, Layer laden, dann erneut öffnen.",
                    level=Qgis.Warning,
                    duration=7)
            return
        self.layer_name_lhw = SETUP.DB_USERS["table"]      
        self.layer_lhw = qgis_toolbox.getLayerHandle(self.layer_name_lhw)
        if not self.layer_ezg:
            self.iface.messageBar().pushMessage(u"Bitte erst Layer bereitstellen. Fenster schließen, Layer laden, dann erneut öffnen.",
                    level=Qgis.Warning,
                    duration=7)
            return
        self.layer_name_m = SETUP.DB_EXTR["table"]      
        self.layer_m = qgis_toolbox.getLayerHandle(self.layer_name_m)
        if not self.layer_ezg:
            self.iface.messageBar().pushMessage(u"Bitte erst Layer bereitstellen. Fenster schließen, Layer laden, dann erneut öffnen.",
                    level=Qgis.Warning,
                    duration=7)
            return  
        self.update_combo()
        event.accept()
        
        
    def update_combo(self):
        """update the combobox"""
        list_id = [feat[SETUP.DB_USERS["id"]] for feat in self.layer_lhw.getFeatures()]
        setCB(self.comboBox_ids, list_id, SETUP.DB_USERS["id"])
        
        
    def delete_entry(self):
        """delete the entries with the ID from EZG and LHW tables"""
        
        selected_id = int(self.comboBox_ids.currentText())

        chk = filter_attr(self.layer_ezg, SETUP.DB_USERS["id"], selected_id)
        ### check if ID is in all layer:
        if not chk:
            self.iface.messageBar().pushMessage(u"Eintrag mit id {} existierte nicht in EZG.".format(selected_id),
                    level=Qgis.Info,
                    duration=3)
        chk = filter_attr(self.layer_m, SETUP.DB_EXTR["id"], selected_id)
        ### check if ID is in all layer:
        if not chk:
            self.iface.messageBar().pushMessage(u"Eintrag mit id {} existierte nicht in Entnahme.".format(selected_id),
                    level=Qgis.Info,
                    duration=3)

        for i, table in enumerate([SETUP.DB_USERS, SETUP.DB_EXTR, SETUP.DB_EZG]):
            layer = [self.layer_lhw, self.layer_m, self.layer_ezg][i]
            with edit(layer):
                # build a request to filter the features based on an attribute
                request = QgsFeatureRequest().setFilterExpression('{} = {}'.format(table["id"], selected_id))
            
                # we don't need attributes or geometry, skip them to minimize overhead.
                # these lines are not strictly required but improve performance
                request.setSubsetOfAttributes([])
                request.setFlags(QgsFeatureRequest.NoGeometry)
            
                # loop over the features and delete
                for f in layer.getFeatures(request):
                    layer.deleteFeature(f.id())
        
            #inDict = {"table": table, "cid": str(selected_id)}
            #self.db_rw.delete_single_entry(inDict, commit=True, print_sql=True)

        self.update_combo()
        print("done")
        
        

