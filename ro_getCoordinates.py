# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Dialog to get coordinates by mouse-click on the map.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

from qgis.gui import QgsMapToolEmitPoint
from PyQt5.QtCore import pyqtSignal
from qgis.PyQt.QtWidgets import QDialog
from qgis.core import QgsProject

from .pyqtDialogs.dialog_RO_coordinates import Ui_Dialog_RO_coordinates
from .setup import setup__ as SETUP
from .tools.iterate_well2 import create_wellPointLayer

DX = {'coordinateEndX':0, 'coordinateEndY': 0}


class PrintClickedPoint(QgsMapToolEmitPoint):
    '''class to get the coorinates from the map'''
    ### speakts to the map
    signalXY = pyqtSignal(list)

    def __init__(self, canvas):
        self.canvas = canvas
        QgsMapToolEmitPoint.__init__(self, self.canvas)

    def canvasPressEvent( self, e ):
        point = self.toMapCoordinates(self.canvas.mouseLastXY())
        point = list(point)
        self.signalXY.emit(point)


class Start_Dialog_RO_getCoordinates(QDialog, Ui_Dialog_RO_coordinates):
    '''class for the dialog'''

    signalXY = pyqtSignal(list)
    signalCoord = pyqtSignal(list)
    signalCoordEP = pyqtSignal(list)
    signalQL = pyqtSignal()

    def __init__(self, parent=None):
        '''do_what ...0 --> wellpoint
           do_what ...1 --> endpoint'''
        QDialog.__init__(self, parent.mainWindow())
        self.setupUi(self)
        self.iface = parent
        self.pushButton.clicked.connect(self.startC)
        self.pushButtonCancel.clicked.connect(self.cancel)
        self.point = [0, 0]
        self.function = 0
        self.xy = PrintClickedPoint(self.iface.mapCanvas())
        self.iface.mapCanvas().setMapTool(self.xy)
        self.xy.signalXY.connect(self.getCoordinates)

    def set_function(self, what):
        self.function = what

    def getCoordinates(self, point):
        '''read the X and Y coordinated from the mask'''
        self.point = point
        self.lineEdit.setText(str(point[0]))
        self.lineEdit_2.setText(str(point[1]))

    def startC(self):
        if self.function == 0:
            self.signalCoord.emit(self.point)
            self.signalQL.emit()
            name = 'Brunnenkoordinaten'
        else:
            self.signalCoordEP.emit(self.point)
            name = 'Endpunkt'

        newWellDict = {'coordinateX': self.point[0],
                       'coordinateY': self.point[1],
                       'x': 0,
                       'ID': -999}
        endp = create_wellPointLayer(newWellDict, crsID=SETUP.CRS,
                                     name=name, geo3='x')
        QgsProject.instance().addMapLayer(endp)

        self.close()

    def cancel(self):
        self.close()

    def closeEvent(self, event):
       event.accept()
