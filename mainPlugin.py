# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Provides the class Main. Top level of the plug-in.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""


import os

from qgis.core import QgsCoordinateReferenceSystem, Qgis, QgsProject

from qgis.PyQt.QtWidgets import QAction, QApplication, QMenu, QToolBar
from qgis.PyQt.QtGui import QIcon

pluginPath = os.path.dirname(__file__)

from .main_RO import Start_Dialog_RO_main
from .main_WB import Start_Dialog_WB_main
from .main_DGs import Start_Dialog_DG_main

from .setup import setup__ as SETUP
from .tools import qgis_toolbox


def load_layers():
    '''load some layers, if they are not already'''
    qgis_toolbox.load_layer(SETUP.EFLLAYER, set_active=False)
    qgis_toolbox.load_layer(SETUP.TGLAYER, set_active=False)
    qgis_toolbox.load_layer(SETUP.AREALAYER, set_active=False)
    qgis_toolbox.load_layer(SETUP.GAUGELAYER, set_active=False)


class Main:
    '''class for the plugin'''
    def __init__(self, iface):
        super(Main, self).__init__()
        ### attributes
        self.iface = iface
        self.tb = None
        self.search_toolbar()
        if SETUP.PRESET_CRS:
            target_crs = QgsCoordinateReferenceSystem()
            target_crs.createFromId(SETUP.CRS)
            iface.mapCanvas().setDestinationCrs(target_crs)
            QgsProject.instance().setCrs(target_crs)
            QApplication.instance().processEvents()
        self.maske0 = Start_Dialog_RO_main(iface)
        self.maske1 = Start_Dialog_WB_main(iface)
        self.maske3 = Start_Dialog_DG_main(iface)
        self.iface.messageBar().pushMessage('Plugin geladen',
                                            level=Qgis.Info, duration=3)

    def initGui(self):
        '''prepares the three entries for (1) reasoned opinion,
        (2) water balance (3) simulations with ArcEGMO(c)'''
        self.actionSn = QAction(QIcon(os.path.join(pluginPath, 'icons', 'ro_st.png')), "Stellungnahmen bearbeiten", self.iface.mainWindow())
        self.actionSn.setWhatsThis("LHW Toolbox: Stellungnahmen bearbeiten")
        self.actionSn.setStatusTip("LHW Toolbox: Stellungnahmen bearbeiten")
        self.actionSn.triggered.connect(self.maske0.show)
        self.tb.addAction(self.actionSn)
        #self.iface.addToolBarIcon(self.actionM)
        self.iface.addPluginToMenu("&LHW Tools", self.actionSn)

        self.actionW = QAction(QIcon(os.path.join(pluginPath, 'icons', 'ro_whh.png')), "Wasserhaushalt", self.iface.mainWindow())
        self.actionW.setWhatsThis("LHW Toolbox: Wasserhaushalt")
        self.actionW.setStatusTip(u"LHW Toolbox: Wasserhaushaltsgrößen für Flächen ausgeben")
        self.actionW.triggered.connect(self.maske1.show)
        self.tb.addAction(self.actionW)
        self.iface.addPluginToMenu("&LHW Tools", self.actionW)
        
        self.actionD = QAction(QIcon(os.path.join(pluginPath, 'icons', 'ro_difga.png')), "DIFGA", self.iface.mainWindow())
        self.actionD.setWhatsThis("LHW Toolbox: DIFGA")
        self.actionD.setStatusTip(u"LHW Toolbox: DIFGA-Ergebnisse für ausgewertete Pegel ausgeben.")
        self.actionD.triggered.connect(self.maske3.show)
        self.tb.addAction(self.actionD)
        self.iface.addPluginToMenu("&LHW Tools", self.actionD)
        
        self.actionM = QAction(QIcon(os.path.join(pluginPath, 'icons', 'ro_map.png')), "Karten LHW Toolbox", self.iface.mainWindow())
        self.actionM.setWhatsThis("LHW Toolbox: DIFGA")
        self.actionM.setStatusTip(u"LHW Toolbox: Kartengrundlagen laden")
        self.actionM.triggered.connect(load_layers)
        self.tb.addAction(self.actionM)
        self.iface.addPluginToMenu("&LHW Tools", self.actionM)
                
        
    def unload(self):
        self.iface.removePluginMenu("&LHW Tools", self.actionSn)
        #self.iface.removeToolBarIcon(self.actionSn)
        self.tb.removeAction(self.actionSn)
                
        self.iface.removePluginMenu("&LHW Tools", self.actionD)
        self.tb.removeAction(self.actionD)
        
        self.iface.removePluginMenu("&LHW Tools", self.actionW)
        self.tb.removeAction(self.actionW)
        
        self.iface.removePluginMenu("&LHW Tools", self.actionM)
        self.tb.removeAction(self.actionM)
        

        #self.iface.removePluginMenu('&LHW Tools', self.startButtonM)
        
    def search_toolbar(self):
        for x in self.iface.mainWindow().findChildren(QToolBar): 
            print(x.windowTitle())
            if x.windowTitle() == 'LHWToolbox':
                self.tb = x
        if not self.tb:
            self.iface.mainWindow().addToolBar('LHWToolbox')
            self.search_toolbar()

