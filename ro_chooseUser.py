# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Dialog to get coordinates by mouse-click on the map.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

from PyQt5.QtCore import pyqtSignal

from qgis.core import QgsProject

from qgis.core import NULL
from qgis.PyQt import QtCore
from qgis.PyQt.QtWidgets import QDialog
from qgis.PyQt.QtWidgets import QTableWidgetItem
from qgis.PyQt.QtGui import QStandardItemModel

from .setup import setup__ as SETUP

from .pyqtDialogs.dialog_RO_chooseUser import Ui_Dialog_choose_perm

DX = {'coordinateEndX':0, 'coordinateEndY': 0}

NUMENTRY = 9


def san(item):
    tmp = '-' if item == NULL else item  ### QPyNullVariant is gone
    return tmp


def check_encoding2(part):
    part = part.replace('\\xf6', '&ouml;')#[2:-1]
    part = part.replace('\\xe4', '&auml')#[2:-1]
    part = part.replace('\\xfc', '&uuml')#[2:-1]
    part = part.replace('\\xdf', '&szlig')#[2:-1]
    return part


class Start_Dialog_chooseUser(QDialog, Ui_Dialog_choose_perm):
    '''class for the dialog'''

    signalXY = pyqtSignal(list)
    signalCoord = pyqtSignal(list)
    signalCoordEP = pyqtSignal(list)
    signalQL = pyqtSignal()

    def __init__(self, parent=None):
        '''do_what ...0 --> wellpoint
           do_what ...1 --> endpoint'''
        QDialog.__init__(self, parent.mainWindow())
        self.setupUi(self)
        self.iface = parent
        self.pushButton.clicked.connect(self.select)
        
        self.layerW = self.iface.activeLayer()
        self.selectedFeatures = self.layerW.selectedFeatures()
        
        self.read_all()
        
        self.nb_row = len(self.cals[0])
        qTable = self.tableWidget
        qTable.setRowCount(self.nb_row)
        qTable.setColumnCount(NUMENTRY)
        chkBoxItem = QTableWidgetItem()
        qTable.setHorizontalHeaderLabels(['Aktenzeichen', 'Name', 'Vorname',
            'u"Rate [Tm³/Jahr]"', 'Zweck', 'Art',
            'Nachtrag', 'Reg.Nr. 1', u'Bearbeiten'])
        
        for row in range(self.nb_row):
            for col in range(8):
                item = QTableWidgetItem(QTableWidgetItem(self.san_t(col, row)))
                qTable.setItem(row, col, item)

            chkBoxItem = QTableWidgetItem()
            chkBoxItem.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
            chkBoxItem.setCheckState(QtCore.Qt.Unchecked)       
            qTable.setItem(row, 8, chkBoxItem)
    
    
    def san_t(self, n, i):
        '''correct encoding for each entry. Nasty hack...'''
        tmp = '?'
        print(self.cals[n][i], n, i)
        if self.cals[n][i] == NULL:
            return '?'
        else:
            try:
                tmp = check_encoding2(str(self.cals[n][i]))
            except:
                tmp = str(self.cals[n][i])
        return tmp
            
    
    def select(self):
        self.layerW.removeSelection()
        for i in range(self.nb_row):
            if self.tableWidget.item(i, NUMENTRY-1).checkState() == QtCore.Qt.Checked:
                self.layerW.selectByIds([self.cals[-1][i]])
                self.close()

                

    def read_all(self):
        
        name = []
        adr_vorname = []
        adr_name = []
        rate = []
        art = []
        zweck = []
        nachtrag = []
        regnr1 = []
        fid = []
        
        layername = self.layerW.name()
        lname = SETUP.DB_CONF[layername]['table']
        efl_layer = QgsProject.instance().mapLayersByName(lname)[0]
        #print('selection for ', layername)
        id_extr = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]["BIL_JAHR_TM3"])
        id_name = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]['applicationNumber'])
        if layername == 'wnv_land':
            id_adr_vorname = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]['userVorName'])
        id_adr_name = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]['userName'])
        
        id_art = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]['nutzart'])
        id_zweck = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]['nutzzweck'])
        id_nachtrag = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]['nachtrag'])
        id_reg_nr2 = efl_layer.fields().lookupField(SETUP.DB_CONF[layername]['regnr1'])

        
        for i, area_feature in enumerate(self.selectedFeatures):
            efl_Attributte = area_feature.attributes()

            fid.append(area_feature.id())

            name.append(san(efl_Attributte[id_name]))
            
            if layername == 'wnv_land':
                tmp = efl_Attributte[id_adr_vorname] if id_adr_vorname > -1 else '-'
            else: 
                tmp = '-'
            adr_vorname.append(san(tmp))
            
            tmp = efl_Attributte[id_adr_name] if id_adr_name > -1 else '-'
            adr_name.append(san(tmp))
            
            tmp = efl_Attributte[id_extr] if id_extr > -1 else '-'
            rate.append(san(tmp))
            
            tmp = efl_Attributte[id_art] if id_art > -1 else '-'
            art.append(san(tmp))
            
            tmp = efl_Attributte[id_zweck] if id_zweck > -1 else '-'
            zweck.append(san(tmp))
            
            tmp = efl_Attributte[id_nachtrag] if id_nachtrag > -1 else '-'
            nachtrag.append(san(tmp))
            
            tmp = efl_Attributte[id_reg_nr2] if id_reg_nr2 > -1 else '-'
            regnr1.append(san(tmp))
            
        selection = [True for i in name]
        self.cals = (name, adr_name, adr_vorname, rate, zweck, art, nachtrag, regnr1, selection, fid)

